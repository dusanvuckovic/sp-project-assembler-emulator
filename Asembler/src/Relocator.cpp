#include "Relocator.h"
#include <iomanip>

namespace assembling {

uword Relocator::currentID = 0;


std::ostream& operator << (std::ostream& stream, const Relocator& r) {
	stream << std::setfill(' ');
	stream << " ";
	stream << std::dec;
	stream << std::setw(3) << std::left << r.relocationID;
	stream << std::setw(12) << std::left << r.symbolName;
	stream << std::setw(11) << std::left << r.sectionID;
	stream << std::setw(12) << std::left << r.sectionName;
	stream << std::setw(8) << std::left << r.relocationOffset;
	stream << std::setw(12) << std::left << r.printType();
	stream << std::endl;
	return stream;


}
} /* namespace assembling */

