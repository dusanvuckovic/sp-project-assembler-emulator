#ifndef _ENDPROGRAMEXCEPTION_H_
#define _ENDPROGRAMEXCEPTION_H_

#include <iostream>
#include <string>
#include <exception>

namespace assembling {

class EndProgramException: public std::exception {

protected:
	std::string errorMessage = "";

public:
	EndProgramException() {
		errorMessage += "Fatal error: program had to be terminated!";
	}

	friend std::ostream& operator << (std::ostream& ostr, const EndProgramException &e) {
		return ostr << e.errorMessage;
	}
};
}

#endif
