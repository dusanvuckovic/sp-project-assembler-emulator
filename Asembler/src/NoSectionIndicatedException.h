#ifndef _NOSECTIONINDICATEDEXCEPTION_H_
#define _NOSECTIONINDICATEDEXCEPTION_H_

#include "EndProgramException.h"

namespace assembling {

class EndProgramException;
class NoSectionIndicatedException final: public EndProgramException {
public:
	NoSectionIndicatedException(): EndProgramException() {

		this->errorMessage += "Input file is incorrect; no section indicated!";
	}
};

} /* namespace assembling */

#endif
