#ifndef _NOTINTEXTSECTIONEXCEPTION_H_
#define _NOTINTEXTSECTIONEXCEPTION_H_

#include "EndProgramException.h"

namespace assembling {
class EndProgramException;

class NotInTextSectionException: public EndProgramException {
public:
	NotInTextSectionException(): EndProgramException() {
		errorMessage += "Code can be added only to a .text section!";
	}
};
}

#endif
