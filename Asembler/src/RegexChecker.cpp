#include "RegexChecker.h"
#include "StackExpressionEvaluator.h"
#include <string>

class StackExpressionEvaluator;

namespace assembling {

const std::string RegexChecker::label = ":"; //only the label may use ':'

const std::vector<std::string> RegexChecker::directives = {".char ", ".byte ", ".word ", ".long ", ".public ", ".extern ", ".skip" , ".end"};

const std::vector<std::string> RegexChecker::operations {"int", "add", "sub", "mul", "div", "cmp", "and", "or", "not", "test", "ldr", "str",
	"call", "in", "out", "mov", "shr", "shl", "ldc", "ldch", "ldcl"};
const std::vector<std::string> RegexChecker::sections = {".data", ".text", ".bss"};
const std::vector<std::string> RegexChecker::registers {"r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13",
	"r14", "r15", "pc", "lr", "sp", "psw"};
const std::vector<std::string> RegexChecker::extensions {"eq", "ne", "gt", "ge", "lt", "le", "al"};

bool RegexChecker::isLabel(const std::string& s) {
	if (s.find(label) != std::string::npos)
		return true;
	return false;
}

bool RegexChecker::isDirective(const std::string& s) {
	for (auto d : directives)
		if (s.substr(0, d.size()).find(d) != std::string::npos)
			return true;
	return false;
}

bool RegexChecker::isOperation(const std::string& s) {
	for (auto o : operations)
		if (s.substr(0, o.size()).find(o) != std::string::npos)
			return true;
	return false;
}

bool RegexChecker::isSection(const std::string& s) {
	for (auto sect : sections)
		if (s.substr(0, sect.size()).find(sect) != std::string::npos)
			return true;
	return false;
}

bool RegexChecker::isRegister(const std::string& s) {
	for (auto r : registers)
		if (s.substr(0, r.size()).find(r) != std::string::npos)
			return true;
	return false;
}

bool RegexChecker::isExtension(const std::string& s) {
	for (auto e: extensions)
		if (s == e)
			return true;
	return false;
}

bool RegexChecker::isImmediate(const std::string& s) {
	unsigned long int* endpointer = nullptr;

	try {
		std::stoi(s, endpointer, 0);
	}
	catch (std::invalid_argument& e) {
		return false;
	}
	catch (std::out_of_range& e) {
		return false;
	}
	return true;
}

bool RegexChecker::isExpression(const std::string& s) {
	for (auto c: s) {
		if (std::isdigit(c) || StackExpressionEvaluator::isOperator(c))
			continue;
		else
			return false;
	}
	return true;
}


}
