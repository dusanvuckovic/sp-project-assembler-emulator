#ifndef _REGEXCHECKER_H_
#define _REGEXCHECKER_H_

#include <regex>
#include <string>

namespace assembling {

class RegexChecker {
private:
	static const std::string label; //only the label may use ':'

	static const std::vector<std::string> directives;
	static const std::vector<std::string> operations;
	static const std::vector<std::string> sections;
	static const std::vector<std::string> registers;
	static const std::vector<std::string> extensions;

public:
	RegexChecker() = delete;
	~RegexChecker() = delete;

	static bool isLabel(const std::string&);
	static bool isDirective(const std::string&);
	static bool isOperation(const std::string&);
	static bool isSection(const std::string&);
	static bool isRegister(const std::string&);
	static bool isExtension(const std::string&);
	static bool isImmediate(const std::string&);
	static bool isExpression(const std::string&);

};
}
#endif
