#include "OperationConstraints.h"

namespace assembling {

OperationConstraints::OperationConstraints() {

	std::deque<Type> d00 { Type::Imm },
	d10 { Type::Reg }, d11 { Type::Reg, Type::Imm, Type::Exp},
	d20 { Type::Reg }, d21 { Type::Reg },
	d30 {Type::Reg }, d31 { Type::Reg}, d32 { Type::Imm, Type::Exp, Type::NoT },
	d40 { Type::Reg, Type::Lab }, d41 { Type::Imm, Type::Exp, Type::Reg, Type::NoT },
	d50 { Type::Reg }, d51 { Type::Reg },
	d60 { Type::Reg }, d61 { Type::Reg}, d62 {Type::Imm, Type::Exp},
	d70 { Type::Reg }, d71 {Type::Lab, Type::Imm, Type::Exp };

	int numberOfGroups = 8;
	int numbersPerGroup[] { 1, 2, 2, 3, 2, 2, 3, 2 };
	std::list<std::deque<Type>> deques { d00, d10, d11, d20, d21, d30, d31, d32,
			d40, d41, d50, d51, d60, d61, d62, d70, d71 };

	auto dequeiterator = deques.begin();
	for (auto i = 0; i < numberOfGroups; i++) {
		int numberInGroup = numbersPerGroup[i];
		for (auto j = 0; j < numberInGroup; j++) {
			allowed[i][j] = std::deque<Type>(*dequeiterator);
			++dequeiterator;
		}
	}

}

} /* namespace assembling */
