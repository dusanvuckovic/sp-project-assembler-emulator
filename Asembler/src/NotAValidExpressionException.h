#ifndef _NOTAVALIDEXPRESSIONEXCEPTION_H_
#define _NOTAVALIDEXPRESSIONEXCEPTION_H_

#include <iostream>
#include "EndProgramException.h"

namespace assembling {

class NotAValidExpressionException final: public EndProgramException {
public:
	NotAValidExpressionException(): EndProgramException() {
		errorMessage += "\nAn assembly expression is not mathematically valid!";
	}
};

} /* namespace assembling */

#endif
