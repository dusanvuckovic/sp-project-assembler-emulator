#include "Symbol.h"
#include <iomanip>
#include <ios>

namespace assembling {

uword Symbol::currentID = 0;

std::ostream& operator << (std::ostream& stream, const Symbol& s) {
	/*	outputFile << "|Identifikator| \t |Ime simbola| \t |Ime sekcije| \t |Pomeraj simbola| \t|Lokalni?| \t |Definisan?|"*/
	stream << " ";
	stream << std::setw(3) << std::left << s.uniqueID;
	stream << std::setw(12) << std::left << s.symbolName;
	stream << std::setw(12) << s.sectionName;
	stream << std::setw(8) << std::to_string(s.symbolOffsetInSection);
	stream << std::setw(9) << std::boolalpha << s.isLocal;
	stream << std::setw(11) << std::boolalpha << s.isDefined;
	stream << s.value << std::endl;
	return stream;
}

} /* namespace assembling */

