#ifndef _SYMBOL_H_
#define _SYMBOL_H_
#include <string>
#include <iostream>
#include "Assembler.h"
#include "Operation.h"

namespace assembling {

class Symbol final {
private:

	static uword currentID;

	uword uniqueID;
	std::string symbolName;
	uword sectionID;
	std::string sectionName;
	uword symbolOffsetInSection;
	bool isLocal;
	bool isDefined;
	bool isExternal;

	word value;

public:
	explicit Symbol(std::string symbolName, uword sectionID, std::string sectionName,
			uword symbolOffsetInSection, bool isLocal, bool isDefined, bool isExternal):
			symbolName(symbolName), sectionID(sectionID), sectionName(sectionName),
			symbolOffsetInSection(symbolOffsetInSection), isLocal(isLocal),
			isDefined(isDefined), isExternal(isExternal), value(0) {
				uniqueID = currentID++;
		}

	Symbol(Symbol&) = delete;
	Symbol(Symbol&&) = delete;
	Symbol& operator = (Symbol&) = delete;
	~Symbol() = default;

	const std::string getSymbolName() const {
		return symbolName;
	}

	const std::string getSectionName() const {
		return sectionName;
	}

	const uword getSymbolOffsetInSection() {
		return symbolOffsetInSection;
	}

	const void setSymbolOffsetInSection(uword offset) {
		symbolOffsetInSection = offset;
	}

	const void setDefined() {
		isDefined = true;
	}

	const bool getIsLocal() const {
		return isLocal;
	}

	const bool getSectionID() const {
		return sectionID;
	}

	void setSectionID(uword sectionID) {
		this->sectionID = sectionID;
	}

	const void setAsGlobal() {
		isLocal = false;
	}

	const word getValue() const {
		return value;
	}

	const bool getIsDefined() {
		return isDefined;
	}

	const void setValue(word myWord) {
		this->value = myWord;
	}

	const void setValue(byte myByte) {
		this->value = myByte;
	}

	const word getID() const {
		return uniqueID;
	}

	void setSectionName(std::string name) {
		sectionName = name;
	}

	void setSectionOffset(uword offset) {
		symbolOffsetInSection = offset;
	}

	void setAsExtern() {
		isExternal = true;
	}

	friend std::ostream& operator << (std::ostream&, const Symbol &s);
};

} /* namespace assembling */

#endif /* SYMBOLTABLEENTRY_H_ */

