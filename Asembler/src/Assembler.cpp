#include "Assembler.h"
#include "Utility.h"
#include <regex>
#include <algorithm>
#include <fstream>
#include "NoSectionIndicatedException.h"
#include <memory>
#include "RegexChecker.h"
#include "Symbol.h"
#include "Section.h"
#include "WrongOperationCodeException.h"
#include "FileNotFoundException.h"
#include "InvalidOperationException.h"
#include "Operation.h"

class InvalidOperationException;
class WrongOperationCodeException;
class BSSSectionNotAllowedException;
class OperationSet;
struct Operand;
namespace assembling {

Assembler::Assembler(std::string inputName, std::string outputName) : currentPass(Pass::NoPass) {
	input = inputName;
	output = outputName;
	std::fstream copier(inputName, std::fstream::in);
	if (copier.fail())
		throw new FileNotFoundException();
	std::string helper;
	while (getline(copier, helper)) {
		Utility::trim(helper);
		if (helper.find(".end") != std::string::npos)
			break;
		if (helper != "")
			lines.push_back(helper);
	}
	copier.close();
	currentSection = nullptr; //vidi ovo
	currentLabel = "";
}

void Assembler::preprocess() {
	std::vector<std::string> newLines;

	for (auto line: lines) {
		if (line.find("iret") != std::string::npos)
			newLines.push_back("movs pc, lr");
		else if (line.find("ret") != std::string::npos)
			newLines.push_back("mov pc, lr");
		else if (line.find("push") != std::string::npos) {
			line = line.substr(5); //preskoči "push "
			line = "strdb sp, " + line;
			newLines.push_back(line);
		}
		else if (line.find("pop") != std::string::npos) {
			line = line.substr(4); //preskoči "pop "
			line = line.substr(0, line.find(' ')); //izbriši sve posle ri (ako ga ima);
			line = "ldria " + line + ", sp";
			newLines.push_back(line);
		}
		else if (line.find("ldc ") != std::string::npos) {
			uword value;
			line = line.substr(4);

			auto position = line.find(",");
			std::string reg = line.substr(0, position);
			line = line.substr(position + 1);
			Utility::trim(line);

			std::string first, second;

			if (RegexChecker::isImmediate(line)) {
				std::istringstream buffer(line);
				if (line.find("0x") != std::string::npos)
					buffer >> std::hex >> value;
				else
					buffer >> value;
				uword upperValue = (value >> 16) & 0xffff;
				uword lowerValue = value & 0xffff;
				first = "ldch " + reg + ", " + std::to_string(upperValue);
				second = "ldcl " + reg + ", " + std::to_string(lowerValue);
			}
			else if (RegexChecker::isExpression(line)) {
				value = StackExpressionEvaluator::evaluateExpression(line);
				uword upperValue = (value >> 16) & 0xffff;
				uword lowerValue = value & 0xffff;
				first = "ldch " + reg + ", " + std::to_string(upperValue);
				second = "ldcl " + reg + ", " + std::to_string(lowerValue);
			}
			else {
				first = "ldch " + reg + ", " + line;
				second = "ldcl " + reg + ", " + line;
			}

			newLines.push_back(first);
			newLines.push_back(second);
		}
		else
			newLines.push_back(line);
	}

	lines = newLines;

	auto position = input.find_last_of(".");
	std::string name = input.substr(0, position) + "_preprocessed.";
	name += input.substr(position + 1);

	std::ofstream copier(name);
	for (auto line: lines)
		copier << line << std::endl;
	copier.close();

}

void Assembler::firstPass() {
	currentPass = Pass::FirstPass;
	for (auto currentLine: lines) {
		if (RegexChecker::isLabel(currentLine))
			if (setLabel(currentLine))
				continue;
		if (RegexChecker::isSection(currentLine))
			setSection(currentLine);
		else if (RegexChecker::isOperation(currentLine))
			setVariables(currentLine);
		else if (RegexChecker::isDirective(currentLine))
			setDirective(currentLine);
		if (currentLine.find_first_not_of(' ') != std::string::npos)
			currentLabel = "";
	}
}

void Assembler::resetLocationCounters() {
	for (auto section: sections)
		section.second->resetOffset();
}


void Assembler::secondPass() {
	currentPass = Pass::SecondPass;
	for (auto currentLine: lines) {
		if (currentLine == "")
			continue;
		if (RegexChecker::isLabel(currentLine))
			if (setLabel(currentLine))
				continue;
		if (RegexChecker::isSection(currentLine))
			setSection(currentLine);
		else if (RegexChecker::isOperation(currentLine))
			setOperation(currentLine);
		else if (RegexChecker::isDirective(currentLine))
			setDirective(currentLine);
		if (currentLine.find_first_not_of(' ') != std::string::npos)
			currentLabel = "";
	}
	addRelocations();
}

bool Assembler::setLabel(std::string& s) {
	if (currentSection->getSectionName() == "")
		throw NoSectionIndicatedException();
	std::string label = s.substr(0, s.find(":")); // this is the label
	s = s.substr(s.find(":") + 1, std::string::npos); //this is the remainder of the string
	Utility::trim(label);
	Utility::trim(s);
	currentLabel = label;
	if (currentPass == Pass::FirstPass) {
		if (!(symbols.count(label))) {
			symbols[label] = new Symbol(label, currentSection->getSectionID(), currentSection->getSectionName(),
					currentSection->getCurrentOffset(), true, true, false);
			relocations.push_back(new Relocator(currentSection->getCurrentOffset(), label,
					currentSection->getSectionID(), currentSection->getSectionName(), RelocationType::R_HYPO_LOAD_LABEL));
		}
		else {
			symbols[label]->setSectionID(currentSection->getSectionID());
			symbols[label]->setSectionName(currentSection->getSectionName());
			symbols[label]->setSectionOffset(currentSection->getCurrentOffset());
			symbols[label]->setDefined();
			relocations.push_back(new Relocator(currentSection->getCurrentOffset(), label,
					currentSection->getSectionID(), symbols[label]->getSectionName(), RelocationType::R_HYPO_LOAD_LABEL));
		}
	}
	if (s.find_first_not_of(' ', 0) == std::string::npos) //if everything after the label is whitespace
		return true;
	return false;
}

void Assembler::setVariables(std::string& source) { //za neku operaciju: source op+cnd
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	for (std::string possibleSymbol: following) {
		Utility::trim(possibleSymbol);
		if (RegexChecker::isRegister(possibleSymbol))
			continue;
		else if (RegexChecker::isImmediate(possibleSymbol))
			continue;
		else if (RegexChecker::isExpression(possibleSymbol))
			continue;
		else if (symbols.count(possibleSymbol))
			continue;
		else
			symbols[possibleSymbol] = new Symbol(possibleSymbol, -1, "", 0, true, false, false);
	}
	currentSection->addToCurrentLocationCounter(4);
}

void Assembler::setOperation(std::string& source) { //format of operations is as follows: <operationCode>, <operand1>, <operand2> where operands can be:
	//registers [ri], natural number expressions with no whitespaces, operations +-*/, and unsigned integer constants (base is irrelevant)
	if (currentSection->getSectionName().find(".text") == std::string::npos)
		throw NotInTextSectionException();
	std::string helper = source;
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	Operation *o = new Operation {helper, source, following,
		currentSection->getSectionName(), currentSection->getSectionID(), currentSection->getSectionName(),
	currentSection->getCurrentOffset()};
	for (Operand& operand: o->getMyOperands()) {
		if (RegexChecker::isRegister(operand.operand))
			operand.type = Type::Reg;
		else if (RegexChecker::isImmediate(operand.operand))
			operand.type = Type::Imm;
		else if (RegexChecker::isExpression(operand.operand))
			operand.type = Type::Exp;
		else if (symbols.count(operand.operand)) {
			operand.type = Type::Lab;
			Symbol *s = symbols[operand.operand];
			operand.setSectionName(s->getSectionName());
			if (o->getOpAttributes().opCode == OperationCode::Call)
				operand.value = symbols[operand.operand]->getSymbolOffsetInSection();
			else
				operand.value = symbols[operand.operand]->getValue();
		}
	}
	currentSection->addToCurrentLocationCounter(4);

	operations.push_back(o);
}

void Assembler::setSection(std::string& source) {
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ' ');
	if (!sections.count(beginning)) {
		currentSection = new Section(beginning);
		sections[currentSection->getSectionName()] = currentSection;
		/*
		 * std::string symbolName, uword sectionID, std::string sectionName,
			uword symbolOffsetInSection, bool isLocal, bool isDefined, bool isExternal
		 * */
		symbols[beginning] = new Symbol(beginning, currentSection->getSectionID(), beginning, 0, true, true, false); //proveri je li pravo ime sekcije/simbola
	}
	else
		currentSection = sections[beginning];
}

void Assembler::setDirective(std::string& source) {
	if (source.find(".char") != std::string::npos || source.find(".byte") != std::string::npos)
		addBytes(source);
	else if (source.find(".public") != std::string::npos)
		addGlobalElements(source);
	else if (source.find(".extern") != std::string::npos)
		addExternElements(source);
	else if (source.find(".word") != std::string::npos)
		addWords(source);
	else if (source.find(".skip") != std::string::npos || source.find(".align") != std::string::npos)
		addSkip(source);
	else if (source.find(".long") != std::string::npos)
		addWords(source);
}

void Assembler::addBytes(std::string& source) { //.byte format: .byte <expr1> <expr2> ... <exprN>
	if (currentSection->getSectionName().find(".bss") != std::string::npos)
		throw BSSSectionNotAllowedException();
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	std::vector<byte> bytesHelper;
	for (std::string symbol: following)
		bytesHelper.push_back(std::stoi(symbol));
	// dodaj proveru vrednosti
	if (currentPass == Pass::FirstPass) {
		symbols[currentLabel]->setDefined();
		symbols[currentLabel]->setSymbolOffsetInSection(currentSection->getCurrentOffset());
		int offset = bytesHelper.size();
		while (offset % 4)
			++offset;
		currentSection->addToCurrentLocationCounter(offset);
	}
	else if (currentPass == Pass::SecondPass) {
		symbols[currentLabel]->setValue(bytesHelper[0]);
		currentSection->addContent(bytesHelper);
		currentSection->pad(4, 0);
	}
}

void Assembler::addGlobalElements(std::string& source) { //.global format: .global <sym1>, <sym2>, ... <symN>
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	for (std::string symbol : following) {
		if (symbols.count(symbol)) //if the symbol exists
			symbols[symbol]->setAsGlobal(); //we name it as a global one
		else  //we add it to the symbol table
			symbols[symbol] = new Symbol(symbol, 0, "", 0, false, false, false);
	}
}

void Assembler::addExternElements(std::string& source) { //.global format: .global <sym1>, <sym2>, ... <symN>
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	for (std::string symbol : following) {
		if (symbols.count(symbol)) //if the symbol exists
			symbols[symbol]->setAsExtern(); //we name it as a global one
		else  //we add it to the symbol table
			symbols[symbol] = new Symbol(symbol, 0, "", 0, true, false, true);
	}
}

void Assembler::addWords(std::string& source) { //.word format: .word <expr1>, <expr2>, ... <exprN>
	if (currentSection->getSectionName().find(".bss") != std::string::npos)
		throw BSSSectionNotAllowedException();
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	std::vector<word> wordsHelper;
	if (following[0].substr(0, 2) == "0x")
		for (std::string symbol: following) {
			int word;
			sscanf(symbol.data(), "%x", &word);
			wordsHelper.push_back(word);
		}
	else
		for (std::string symbol : following)
			wordsHelper.push_back(std::stoi(symbol)); // dodaj proveru vrednosti
	if (currentPass == Pass::FirstPass) {
		symbols[currentLabel]->setDefined();
		symbols[currentLabel]->setSymbolOffsetInSection(currentSection->getCurrentOffset());
		int offset = wordsHelper.size() * 4;
		currentSection->addToCurrentLocationCounter(offset);
	}
	else if (currentPass == Pass::SecondPass) {
		symbols[currentLabel]->setValue(wordsHelper[0]);
		currentSection->addContent(wordsHelper);
	}
}

void Assembler::addSkip(std::string& source) {
	if (currentPass == Pass::SecondPass)
		return;
	Utility::split(source, beginning, ' ');
	Utility::split(beginning, following, ',');
	unsigned int size = std::stoi(following[0]);
	byte fill = 0;
	if (following.size() == 2) //if there is specified fill
		fill = std::stoi(following[1]); //we take it; else, zero
	currentSection->fill(size, fill);
	currentSection->pad(4, 0);
}

void Assembler::addRelocations() {
	for (Operation* o: operations)
		o->processOperation();
	for (Operation *o: operations)
		for (Operand& op: o->getMyOperands())
			if (op.message == OperandMessage::EmitRelocation) {
				word offset = o->getOperationOffset();
				std::string symName = (o->getOpAttributes().opCode == OperationCode::Call) ?
						o->getMyOperands()[0].operand : o->getMyOperands()[1].operand;
				uword sectionID = o->getSectionID();
				std::string sectionName = o->getSectionName();
				RelocationType rt;
				if (o->getOpAttributes().opCode == OperationCode::Ldch)
					rt = RelocationType::R_HYPO_LINK_LDCH;
				else if (o->getOpAttributes().opCode == OperationCode::Ldcl)
					rt = RelocationType::R_HYPO_LINK_LDCL;
				else if (o->getOpAttributes().opCode == OperationCode::Call)
					rt = RelocationType::R_HYPO_LINK_CALL;
				relocations.push_back(new Relocator(offset, symName, sectionID, sectionName, rt));
			}
			else if (op.message == OperandMessage::InsertLiteral) {
				word offset = o->getOperationOffset();
				Section *s = sections[o->getSectionName()];
				word maxSize = s->getCurrentOffset();
			}

		for (Operation* o: operations) {
			Section *s = sections.at(o->getOperationSection());
			uword wrd = o->getBitset(), offset = o->getOperationOffset();
			s->addOperation(wrd, offset);
		}
}

bool symbolCompare(Symbol const *a, Symbol const *b) {
    return a->getID() < b->getID();
}

void Assembler::writeOut() {
	std::string outputString;
	std::fstream outputFile(output, std::fstream::out|std::fstream::trunc);
	if (outputFile.fail())
		throw new FileNotFoundException();
	outputFile << "Rezultat asembliranja ulaznog fajla '" << input << "'!" << std::endl;
	outputFile << std::endl;

	outputFile << "#################################################################" << std::endl;
	outputFile << "Tabela simbola:" << std::endl;
	outputFile << "|ID|Ime simbola|Ime sekcije|Pomeraj|Lokalni?|Definisan?|Vrednost|" << std::endl;
	outputFile << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
	std::vector<Symbol*> syms;
		for (auto symbol: symbols)
			syms.push_back(symbol.second);
		std::sort(syms.begin(), syms.end(), symbolCompare);
	for (auto symbol: syms)
		outputFile << *symbol;
	outputFile << "#################################################################" << std::endl;
	outputFile << std::endl;

	outputFile << "###########################" << std::endl;
	outputFile << "Sekcije:" << std::endl;
	outputFile << "|ID|Ime sekcije|Dužina (B)|" << std::endl;
	outputFile << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
	for (auto section: sections) {
		outputFile << *section.second;
		outputFile << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
	}
	outputFile << "###########################" << std::endl;
	outputFile << std::endl;

	outputFile << "########################################################" << std::endl;
	outputFile << "Tabela relokacija:" << std::endl;
	outputFile << "|ID|Ime simbola|ID sekcije|Ime sekcije|Pomeraj|Relok. tip|" << std::endl;
	outputFile << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
	for (auto r: relocations) {
		outputFile << *r;
		outputFile << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
	}
	outputFile << "########################################################" << std::endl;
	outputFile.close();


}

} /* namespace assembling */
