#ifndef INVALIDOPERATIONEXCEPTION_H_
#define INVALIDOPERATIONEXCEPTION_H_

#include <exception>
#include "Operation.h"

class Operation;
namespace assembling {


class InvalidOperationException: public EndProgramException {
public:
	InvalidOperationException(Operation& o): EndProgramException() {

		errorMessage += "\nIn the line " + o.getFullLine() + "following operands are disallowed:";
		for (auto& operand: o.getMyOperands())
			errorMessage += "\n" + operand.operand;
	}
};

} /* namespace assembling */

#endif /* INVALIDOPERATIONEXCEPTION_H_ */
