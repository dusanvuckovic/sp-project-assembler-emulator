#ifndef OPERATION_H_
#define OPERATION_H_

#include <bitset>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include "Section.h"
#include "Assembler.h"
#include "Symbol.h"
#include "Utility.h"
#include "OperationConstraints.h"

namespace assembling {

class BackpatchEntry;
class SymbolTableEntry;
class Compare;

enum class OperationCode {
	Int, Add, Sub, Mul, Div, Cmp, And, Or, Not, Test, Ldr, Str, Call, In, Out, Mov, Shr, Shl, Ldc, Ldch, Ldcl, Err
};

enum class OperationFlags {
	NoFlagUpdate, FlagUpdate, IncrementBefore, IncrementAfter, DecrementBefore, DecrementAfter, Err
};

enum class OperationExecution {
	No, Eq, Ne, Gt, Ge, Lt, Le, Reserved, Al, Err
};

using Mask = uword;
using Group = uword;

struct OperationAttributes {
	OperationCode opCode = OperationCode::Err;
	OperationFlags opFlags = OperationFlags::Err;
	OperationExecution opExec = OperationExecution::Err;
};

enum class OperandMessage {
	NoError, InsertLiteral, EmitRelocation
};

struct Operand {
	std::string operand;
	bool found = false;
	word value = 0;
	uword bitsize = 0;
	Type type;
	std::string sectionName = "";
	OperandMessage message = OperandMessage::NoError;
	Operand(std::string o) {
		operand = o;
		type = Type::Err;
	}
	void setSectionName(std::string name) {
		sectionName = name;
	}
};

class Operation final {
private:
	Group myGroup = 8;
	OperationAttributes myAttributes;
	std::string fullLine;
	std::string operation;
	std::string operationSection;
	uword sectionID = 0;
	std::string sectionName;
	uword operationOffset = 0;

	uword myBitset = 0;
	uword howManyFound = 0;
	std::deque<Operand> myOperands;
	std::deque<std::string> operands;
	bool isComplete = false;

	static OperationConstraints myConstraints;

	static OperationCode getOpcodeFromName(std::string& s);
	static OperationFlags getOpflagsFromName(std::string& s);
	static OperationExecution getOpexecFromName(std::string& s);

public:
	Operation(std::string line, std::string instruction, std::vector<std::string>& operands,
			std::string section, uword sectionID, std::string sectionName, uword sectionOffset);
	Operation(Operation&) = delete;
	Operation& operator = (Operation&) = delete;
	~Operation() = default;


	bool getIsComplete() const {
		return isComplete;
	}

	const std::string& getFullLine() const {
		return fullLine;
	}
	std::deque<Operand>& getMyOperands();

	OperationAttributes getOpAttributes() {
		return myAttributes;
	}

	static OperationAttributes getOpAttributesFromName(std::string);
	static bool areParametersAllowed(Group group, uword number, Type type);
	static Group getGroupFromOpcode(const OperationCode);
	static uword getBitsetFromName(const std::string);

	void doRegisterName();
	void doSimpleNumber();
	void doStackEvaluation();


	uword getOperationOffset() const {
		return operationOffset;
	}

	uword getSectionID() const {
		return sectionID;
	}

	std::string getSectionName() const {
		return sectionName;
	}
	const std::string& getOperationSection() const {
		return operationSection;
	}

	uword getBitset() const {
		return myBitset;
	}

	Group getMyGroup() const {
		return myGroup;
	}

	static bool inline isRegisterName(std::string&);

	void processOperation();
	void processLabel(Operand& o);

	friend void f0(Operation&);
	friend void f1(Operation&);
	friend void f2(Operation&);
	friend void f3(Operation&);
	friend void f4(Operation&);
	friend void f5(Operation&);
	friend void f6(Operation&);
	friend void f7(Operation&);
};


}
#endif
