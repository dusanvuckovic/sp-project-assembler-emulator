#include "Section.h"
#include <ios>
#include <iomanip>
#include <bitset>

namespace assembling {

uword Section::currentID = 0;

std::ostream& operator <<(std::ostream& stream, const Section& s) {
	stream << " ";
	stream << std::setfill(' ') << std::dec;
	stream << std::setw(3) << std::left << s.sectionID;
	stream << std::setw(12) << std::left << s.sectionName;
	stream << std::setw(12) << std::left << s.totalOffset;
	stream << std::endl;
	stream << "Podaci:" << std::endl;
	int byteCounter = 0, rowCounter = 0;
	uint32_t output = 0;
	for (unsigned char b : s.content) {
		output |= b << (24 - 8 * byteCounter++);
		if (byteCounter == 4) {
			byteCounter = 0;
			for (int i = 0; i < 4; i++) {
				stream << std::hex << std::setfill('0') << ((output >> (24 - (i*8))&0xff));
				if (i != 3)
					stream << " ";
				else
					stream << "|";
			}
			output = 0;
			if (++rowCounter == 3) {
				rowCounter = 0;
				stream << std::endl;
			}

		}
	}
	stream << std::setfill(' ');

	stream << std::endl;
	return stream;
}

} /* namespace assembling */
