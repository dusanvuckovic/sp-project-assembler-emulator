#ifndef STACKEXPRESSIONERROREXCEPTION_H_
#define STACKEXPRESSIONERROREXCEPTION_H_

#include "EndProgramException.h"

namespace assembling {

class StackExpressionErrorException: public EndProgramException {
public:

	StackExpressionErrorException() : EndProgramException() {
		errorMessage += "An error occured while processing an expression!";
	}

};

} /* namespace assembling */

#endif /* STACKEXPRESSIONERROREXCEPTION_H_ */
