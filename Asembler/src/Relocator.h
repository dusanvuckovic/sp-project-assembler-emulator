#ifndef _RELOCATOR_H_
#define _RELOCATOR_H_
#include <string>
#include <map>
#include "Symbol.h"
#include "Utility.h"

namespace assembling {

enum class RelocationType {
	R_HYPO_LOAD_LABEL, R_HYPO_LINK_CALL, R_HYPO_LINK_LDCH, R_HYPO_LINK_LDCL
};

const std::map<RelocationType, std::string> printMap {

	{RelocationType::R_HYPO_LOAD_LABEL, "R_HYPO_LOAD_LABEL"},
	{RelocationType::R_HYPO_LINK_CALL, "R_HYPO_LINK_CALL"},
	{RelocationType::R_HYPO_LINK_LDCH, "R_HYPO_LINK_LDCH"},
	{RelocationType::R_HYPO_LINK_LDCL, "R_HYPO_LINK_LDCL"}

};

class Relocator final {

private:
	static uword currentID;

	uword relocationID;
	uword sectionID; //broj sekcije koju menjamo
	uword relocationOffset; //pozicija u sekciji
	std::string symbolName;
	std::string sectionName;
	RelocationType relocationType; //apsolutna ili

public:

	Relocator(word offset, std::string sn, uword sectionID, std::string sectionName, RelocationType rt):
		 sectionID(sectionID), relocationOffset(offset), symbolName(sn), sectionName(sectionName), relocationType(rt) {
		relocationID = currentID++;
	}

	Relocator(Relocator&) = delete;
	Relocator& operator = (Relocator&) = delete;
	Relocator(Relocator&&) = delete;
	~Relocator() = default;
	std::string printType() const {
		return printMap.at(relocationType);
	}
	friend std::ostream& operator << (std::ostream&, const Relocator&);
};
}

#endif
