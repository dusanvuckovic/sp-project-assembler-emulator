#ifndef _FILENOTFOUNDEXCEPTION_H_
#define _FILENOTFOUNDEXCEPTION_H_

#include <iostream>
#include "EndProgramException.h"

namespace assembling {

class EndProgramException;
class FileNotFoundException final: public EndProgramException {
public:
	FileNotFoundException(): EndProgramException() {
		this->errorMessage += "\nAn error occurred while opening a file!";
	}
};

} /* namespace assembling */

#endif
