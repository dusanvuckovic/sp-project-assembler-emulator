#ifndef _WRONGOPERATIONCODEEXCEPTION_H_
#define _WRONGOPERATIONCODEEXCEPTION_H_

#include "EndProgramException.h"

namespace assembling {

class WrongOperationCodeException final: public EndProgramException {
public:
	WrongOperationCodeException(): EndProgramException() {
		this->errorMessage += "\nThe operation code specified does not exist!";
	}
};

} /* namespace assembling */

#endif
