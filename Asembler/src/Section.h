#ifndef _SECTION_H_
#define _SECTION_H_
#include <iostream>
#include <string>
#include <map>
#include <cstdint>
#include <vector>

namespace assembling {

using hword = int16_t;
using word = int32_t;
using uword = uint32_t;
using byte = uint8_t;

using Offset = uword;
class Section final {
private:
	static uword currentID;

	std::string sectionName;
	uword currentOffset;
	uword totalOffset;
	std::vector<byte> content;
	uword sectionID;

public:
	Section(std::string ILsectionName) :
			sectionName(ILsectionName), currentOffset(0), totalOffset(0) {
		sectionID = currentID++;
	}
	Section(Section&) = delete;
	Section(Section&&) = delete;
	Section& operator =(Section&) = delete;
	~Section() = default;

	void addContent(std::vector<byte>& contentToBeAdded) {
		for (auto by : contentToBeAdded)
			content.push_back(by);
		currentOffset += contentToBeAdded.size();
	}

	void addContent(std::vector<word>& contentToBeAdded) {
		for (uword i = 0; i < contentToBeAdded.size(); i++) {
			word w = contentToBeAdded[i];
			byte bytes[4] {
				static_cast<byte>((w >> 24) & 0xff),
				static_cast<byte>((w >> 16) & 0xff),
				static_cast<byte>((w >> 8) & 0xff),
				static_cast<byte>(w & 0xff)
			};
			for (uword i = 0; i < 4; i++) {
				byte b = bytes[i];
				content.push_back(b);
			}
		}
		currentOffset += contentToBeAdded.size() * sizeof(word);
	}

	void addOperation(uword w, uword startingPosition) {
		byte bytes[4] {
			static_cast<byte>((w >> 24) & 0xff),
			static_cast<byte>((w >> 16) & 0xff),
			static_cast<byte>((w >> 8) & 0xff),
			static_cast<byte>(w & 0xff)
		};
		for (uword i = 0; i < 4; i++) {
			byte b = bytes[i];
			content.push_back(b);
		}
	}

	void addContent(std::vector<word>& contentToBeAdded,
			uword startingPosition) {
		for (uword i = 0; i < contentToBeAdded.size(); i++) {
			word w = contentToBeAdded[i];
			byte bytes[4] {
				static_cast<byte>((w >> 24) & 0xff),
				static_cast<byte>((w >> 16) & 0xff),
				static_cast<byte>((w >> 8) & 0xff),
				static_cast<byte>(w & 0xff)
			};
			for (uword i = 0; i < 4; i++) {
				byte b = bytes[i];
				content[startingPosition++] = b;
			}
		}
	}

	void fill(const int numberOfLocations, const word fillValue) {
		for (int i = 0; i < numberOfLocations; i++) {
			content.push_back(fillValue);
			++currentOffset;
		}
	}

	void pad(const int alignParameter, const word alignValue) {
		while (currentOffset % alignParameter) {
			content.push_back(alignValue);
			++currentOffset;
		}
	}

	const Offset getCurrentOffset() const {
		return currentOffset;
	}

	const std::string getSectionName() const {
		return sectionName;
	}

	void addToCurrentLocationCounter(const uword addition) {
		currentOffset += addition;
	}

	void resetOffset() {
		totalOffset = currentOffset;
		currentOffset = 0;
	}

	uword getSectionID() const {
		return sectionID;
	}

	friend std::ostream& operator <<(std::ostream& stream,
			const Section& section);

};

}
#endif
