#include "Operation.h"
#include "StackExpressionEvaluator.h"
#include <vector>
#include <string>
#include "Assembler.h"
#include <unordered_map>
#include <map>

namespace assembling {

OperationConstraints Operation::myConstraints { };

const std::map<OperationCode, uword> max_bit_lengths {
	{OperationCode::Int, 1<<4},
	{OperationCode::Add, 1<<18}, {OperationCode::Sub, 1<<18},
	{OperationCode::Mul, 1<<18}, {OperationCode::Div, 1<<18}, {OperationCode::Cmp, 1<<18},
	{OperationCode::Ldr, 1<<10}, {OperationCode::Str, 1<<10},
	{OperationCode::Call, 1<<19}, {OperationCode::Ldr, 1<<19},
	{OperationCode::Shl, 1<<10}, {OperationCode::Shr, 1<<10},
	{OperationCode::Ldch, 1<<16}, {OperationCode::Ldcl, 1<<16},
};

using OperationFunction = void (*)(Operation&);

const std::vector<uword> enableTimer = { 0x5c400000, //push r2
		0x5c600000, //push r3
		0x5e000000, //push psw
		0x60400000, //pop r2
		0x1ccc0000, //not r3, r3
		0x00cc0001, //add r3, r3, 1
		0x74cc0036, //shl r3, r3, 30
		0x18cc0000, //or r2, r3, 0
		0x5c400000, //push r2
		0x62000000, //pop psw
		0x60600000, //pop r3
		0x60400000 //pop r2
		};

const std::vector<std::string> registers = { "r0", "r1", "r2", "r3", "r4", "r5",
		"r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "pc",
		"lr", "sp", "psw" };

const std::unordered_map<std::string, uword> register_to_bitset = { { "r0", 0 },
		{ "r1", 1 }, { "r2", 2 }, { "r3", 3 }, { "r4", 4 }, { "r5", 5 }, { "r6",
				6 }, { "r7", 7 }, { "r8", 8 }, { "r9", 9 }, { "r10", 10 }, {
				"r11", 11 }, { "r12", 12 }, { "r13", 13 }, { "r14", 14 }, {
				"15", 15 }, { "pc", 16 }, { "lr", 17 }, { "sp", 18 }, { "psw",
				19 } };

const std::map<OperationCode, Group> opcode_to_group = {
		{ OperationCode::Int, 0 }, { OperationCode::Add, 1 }, {
				OperationCode::Sub, 1 }, { OperationCode::Mul, 1 }, {
				OperationCode::Div, 1 }, { OperationCode::Cmp, 1 }, {
				OperationCode::And, 2 }, { OperationCode::Or, 2 }, {
				OperationCode::Not, 2 }, { OperationCode::Test, 2 }, {
				OperationCode::Ldr, 3 }, { OperationCode::Str, 3 }, {
				OperationCode::Call, 4 }, { OperationCode::In, 5 }, {
				OperationCode::Out, 5 }, { OperationCode::Mov, 6 }, {
				OperationCode::Shr, 6 }, { OperationCode::Shl, 6 }, {
				OperationCode::Ldc, 7 }, { OperationCode::Ldcl, 7 }, {
				OperationCode::Ldch, 7 } };

const std::map<OperationExecution, uword> extension_to_bitset { {
		OperationExecution::Eq, 0 }, { OperationExecution::Ne, 1 }, {
		OperationExecution::Gt, 2 }, { OperationExecution::Ge, 3 }, {
		OperationExecution::Lt, 4 }, { OperationExecution::Le, 5 }, {
		OperationExecution::Al, 7 }, { OperationExecution::No, 8 } };

const std::unordered_map<std::string, uword> name_to_bitset = { { "int", 0 }, {
		"add", 1 }, { "sub", 2 }, { "mul", 3 }, { "div", 4 }, { "cmp", 5 }, {
		"and", 6 }, { "or", 7 }, { "not", 8 }, { "test", 9 }, { "ldr", 10 }, {
		"str", 10 }, { "call", 12 }, { "in", 13 }, { "out", 13 }, { "mov", 14 },
		{ "shr", 14 }, { "shl", 14 }, { "ldc", 15 }, { "ldch", 15 }, { "ldcl",
				15 } };

const std::map<std::string, OperationCode> name_to_opcode = { { "int",
		OperationCode::Int }, { "add", OperationCode::Add }, { "sub",
		OperationCode::Sub }, { "mul", OperationCode::Mul }, { "div",
		OperationCode::Div }, { "cmp", OperationCode::Cmp }, { "and",
		OperationCode::And }, { "or", OperationCode::Or }, { "not",
		OperationCode::Not }, { "test", OperationCode::Test }, { "ldr",
		OperationCode::Ldr }, { "str", OperationCode::Str }, { "call",
		OperationCode::Call }, { "in", OperationCode::In }, { "out",
		OperationCode::Out }, { "mov", OperationCode::Mov }, { "shr",
		OperationCode::Shr }, { "shl", OperationCode::Shl }, { "ldc",
		OperationCode::Ldc }, { "ldch", OperationCode::Ldch }, { "ldcl",
		OperationCode::Ldcl } };
const std::vector<std::string> op_names = { "int", "add", "sub", "mul", "div",
		"cmp", "and", "or", "not", "test", "ldr", "str", "call", "in", "out",
		"mov", "shr", "shl", "ldch", "ldcl", "ldc"};

const std::map<OperationCode, uword> opcode_to_bitset = { { OperationCode::Int,
		0 }, { OperationCode::Add, 1 }, { OperationCode::Sub, 2 }, {
		OperationCode::Mul, 3 }, { OperationCode::Div, 4 }, {
		OperationCode::Cmp, 5 }, { OperationCode::And, 6 }, { OperationCode::Or,
		7 }, { OperationCode::Not, 8 }, { OperationCode::Test, 9 }, {
		OperationCode::Ldr, 10 }, { OperationCode::Str, 10 }, {
		OperationCode::Call, 12 }, { OperationCode::In, 13 }, {
		OperationCode::Out, 13 }, { OperationCode::Mov, 14 }, {
		OperationCode::Shr, 14 }, { OperationCode::Shl, 14 }, {
		OperationCode::Ldc, 15 }, { OperationCode::Ldch, 15 }, {
		OperationCode::Ldcl, 15 } };

const std::map<OperationCode, std::string> opcode_to_name = { {
		OperationCode::Int, "int" }, { OperationCode::Add, "add" }, {
		OperationCode::Sub, "sub" }, { OperationCode::Mul, "mul" }, {
		OperationCode::Div, "div" }, { OperationCode::Cmp, "cmp" }, {
		OperationCode::And, "and" }, { OperationCode::Or, "or" }, {
		OperationCode::Not, "not" }, { OperationCode::Test, "test" }, {
		OperationCode::Ldr, "ldr" }, { OperationCode::Str, "str" }, {
		OperationCode::Call, "call" }, { OperationCode::In, "in" }, {
		OperationCode::Out, "out" }, { OperationCode::Mov, "mov" }, {
		OperationCode::Shr, "shr" }, { OperationCode::Shl, "shl" },
		{ OperationCode::Ldch, "ldch" }, {OperationCode::Ldcl, "ldcl" }, {OperationCode::Ldc, "ldc" } };

const std::map<std::string, OperationExecution> extension_to_opext = { { "eq",
		OperationExecution::Eq }, { "ne", OperationExecution::Ne }, { "gt",
		OperationExecution::Gt }, { "ge", OperationExecution::Ge }, { "lt",
		OperationExecution::Lt }, { "le", OperationExecution::Le }, { "al",
		OperationExecution::Al }
};


Operation::Operation(std::string line, std::string instruction,
		std::vector<std::string>& operandsV, std::string section,
		uword sectionID, std::string sectionName, uword sectionOffset) :
		fullLine(line), operation(instruction), operationSection(section), sectionID(
				sectionID), sectionName(sectionName), operationOffset(sectionOffset) {
	myAttributes = getOpAttributesFromName(instruction);
	myGroup = getGroupFromOpcode(myAttributes.opCode);
	for (auto operand : operandsV) {
		Utility::trim(operand);
		myOperands.push_back(operand);
		operands.push_back(operand);
	}
}

void f0(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0xf) << 20);
}

void f1(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0x1f) << 19);
	if (operation.myOperands[1].type == Type::Imm || operation.myOperands[1].type == Type::Exp) {
		operation.myBitset |= (1 << 18); //na poziciji 18 je jedan
		operation.myBitset |= (((operation.myOperands[1].value) & 0x3ffff));
	}
	else
		operation.myBitset |= (((operation.myOperands[1].value) & 0x1f) << 13);
}

void f2(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0x1f) << 19);
	operation.myBitset |= (((operation.myOperands[1].value) & 0x1f) << 14);

}

void f3(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0x1f) << 19);
	operation.myBitset |= (((operation.myOperands[1].value) & 0x1f) << 14);
	uword f = 0;
	switch (operation.myAttributes.opFlags) {
	case OperationFlags::DecrementBefore:
		f = 5;
		break;
	case OperationFlags::IncrementBefore:
		f = 4;
		break;
	case OperationFlags::DecrementAfter:
		f = 3;
		break;
	case OperationFlags::IncrementAfter:
		f = 2;
		break;
	default:
		break;
	}
	if (operation.myAttributes.opCode == OperationCode::Ldr)
		operation.myBitset |= (1 << 10);
	if (operation.myOperands[0].value == 16) //ako je pc
		f = 0; //override inc.
	if (operation.myOperands[0].value == 19)
		exit(-1);
	operation.myBitset |= (f << 11);
	operation.myBitset |= (operation.myOperands[2].value & 0x3ff);
}

void f4(Operation& operation) {
	operation.myBitset |= ((uword)16) << 19;

	//ako je u istoj sekciji
	if (operation.getSectionName() == operation.myOperands[0].sectionName) {
		word value = operation.myOperands[0].value - operation.operationOffset - 4;
		word maxValue = max_bit_lengths.at(OperationCode::Call);
		word maxValueUpper = (maxValue / 2) - 1;
		word maxValueLower = -(maxValue / 2);
		if (value >= maxValueLower && value <= maxValueUpper) {
			word valueFW = value & 0x3ffff;
			operation.myBitset |= valueFW;
		}
		else {
			operation.myOperands[0].message = OperandMessage::InsertLiteral;
		}
	}
	else
		operation.myOperands[0].message = OperandMessage::EmitRelocation;
}

void f5(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0xf) << 20);
	operation.myBitset |= (((operation.myOperands[1].value) & 0xf) << 16);
	if (operation.myAttributes.opCode == OperationCode::In)
		operation.myBitset |= (1 << 15);
}

void f6(Operation& operation) {
	operation.myBitset |= (((operation.myOperands[0].value) & 0x1f) << 19);
	operation.myBitset |= (((operation.myOperands[1].value) & 0x1f) << 14);
	operation.myBitset |= (((operation.myOperands[2].value) & 0x1f) << 9);
	if (operation.myAttributes.opCode == OperationCode::Shl)
		operation.myBitset |= (1 << 8);

	operation.myBitset &= 0xffffff00;
}

void f7(Operation& operation) {
	operation.myBitset |= operation.myOperands[0].value << 20;
	if (operation.myOperands[1].type == Type::Lab)
		operation.myOperands[1].message = OperandMessage::EmitRelocation;
	else {
		if (operation.myAttributes.opCode == OperationCode::Ldch)
			operation.myBitset |= 1 << 19;
		operation.myBitset |= (operation.myOperands[1].value) & 0xffff;
	}
}

const std::map<uword, OperationFunction> operations = { { 0, f0 }, { 1, f1 }, {
		2, f2 }, { 3, f3 }, { 4, f4 }, { 5, f5 }, { 6, f6 }, { 7, f7 } };

Group Operation::getGroupFromOpcode(const OperationCode o) {
	return opcode_to_group.at(o);
}

bool checkIfExtension(std::string& s) {
	return RegexChecker::isExtension(s);
}

OperationCode Operation::getOpcodeFromName(std::string& s) {
	OperationCode helper = OperationCode::Err;
	std::string current_op;
	int current_op_size;
	for (auto& operation : op_names) {
		std::string hold = s.substr(0, operation.size());
		if (hold == operation) {
			current_op = operation;
			current_op_size = operation.size();
			break;
		}
	}

	helper = name_to_opcode.at(current_op);
	s = s.substr(current_op_size); //'iseci' deo sa nazivom
	return helper;
}

OperationExecution Operation::getOpexecFromName(std::string& s) {
	OperationExecution myExec = OperationExecution::Al;

	auto possibleExtension = s.substr(0, 2);
	if (RegexChecker::isExtension(possibleExtension)
			&& extension_to_opext.count(possibleExtension)) {
		myExec = extension_to_opext.at(possibleExtension);
		s = s.substr(2);
	}
	return myExec;
}

OperationFlags Operation::getOpflagsFromName(std::string& s) {
	OperationFlags myFlags = OperationFlags::NoFlagUpdate;
	if (s == "s") {
		myFlags = OperationFlags::FlagUpdate;
		s = s.substr(1);
	}

	if (s == "ia")
		myFlags = OperationFlags::IncrementAfter;
	else if (s == "ib")
		myFlags = OperationFlags::IncrementBefore;
	else if (s == "da")
		myFlags = OperationFlags::DecrementAfter;
	else if (s == "db")
		myFlags = OperationFlags::DecrementBefore;
	return myFlags;
}

OperationAttributes Operation::getOpAttributesFromName(std::string s) {

	OperationAttributes oa;

	oa.opCode = getOpcodeFromName(s);
	oa.opExec = getOpexecFromName(s);
	oa.opFlags = getOpflagsFromName(s);

	if (oa.opCode == OperationCode::Cmp || oa.opCode == OperationCode::Test)
		oa.opFlags = OperationFlags::FlagUpdate;
	return oa;
}

bool Operation::areParametersAllowed(Group group, uword number, Type type) {
	return Operation::myConstraints.isAllowed(group, number, type);
}

uword Operation::getBitsetFromName(const std::string s) {
	return name_to_bitset.at(s);
}

std::deque<Operand>& Operation::getMyOperands() {
	return myOperands;
}


void Operation::processOperation() {
	for (uword i = 0; i < myOperands.size(); i++)
		if (!areParametersAllowed(myGroup, i, myOperands[i].type))
			myOperands[i].type = Type::Err;

	isComplete = true;

	for (auto& operand : myOperands)
		switch (operand.type) {
		case Type::Reg:
			operand.value = register_to_bitset.at(operand.operand);
			break;
		case Type::Exp:	case Type::Imm:
			if (operand.type == Type::Exp)
				operand.value = StackExpressionEvaluator::evaluateExpression(operand.operand);
			else
				if (operand.operand.find("0x") != std::string::npos) {
					std::stringstream ss(operand.operand);
					ss >> std::hex >> operand.value;
				}
				else
					operand.value = std::stoi(operand.operand);

				if (operand.value > (word)max_bit_lengths.at(myAttributes.opCode))
					exit(-3);
			break;
		case Type::Lab:
			isComplete = false;
			continue;
		case Type::NoT:
			continue;
		case Type::Err:
			break;
		}

	uword myBitsetExtension = extension_to_bitset.at(myAttributes.opExec);
	uword mySignExtension =
			(myAttributes.opFlags == OperationFlags::FlagUpdate) ? 1 : 0;
	uword myOperationExtension = opcode_to_bitset.at(myAttributes.opCode);

	myBitset |= ((myBitsetExtension & 0x7) << 29);
	myBitset |= ((mySignExtension & 0x1) << 28);
	myBitset |= ((myOperationExtension & 0xf) << 24);

	((*operations.at(myGroup)))(*this);

}

} /*namespace assembling */
