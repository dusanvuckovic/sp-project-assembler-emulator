#ifndef _BSSSECTIONNOTALLOWEDEXCEPTION_H_
#define _BSSSECTIONNOTALLOWEDEXCEPTION_H_

#include <iostream>
#include "EndProgramException.h"

namespace assembling {

class BSSSectionNotAllowedException final: public EndProgramException {
public:
	BSSSectionNotAllowedException(): EndProgramException() {
		this->errorMessage += "\nData cannot be initialized in a .bss section!";
	}
};

} /* namespace assembling */

#endif
