#include "Utility.h"
#include <string>
namespace assembling {

void Utility::trim(std::string& currentLine) {
	if (currentLine.find("@") != std::string::npos)
		currentLine = currentLine.substr(0, currentLine.find("@"));
	if (currentLine.find("#") != std::string::npos)
			currentLine = currentLine.substr(0, currentLine.find("#"));
	while (currentLine.front() == '\t' || currentLine.front() == ' ')
		currentLine.erase(0, 1); //erase it
	while (currentLine.back() == '\t' || currentLine.back() == ' ')
		currentLine.erase(currentLine.length() - 1, 1); //erase it
}
void Utility::split(std::string& arguments, std::vector<std::string>& symbols, const char delimiter) {
	std::string::size_type iterate = 0;
	std::string::size_type delimiterLocation = arguments.find(delimiter);
	symbols.clear();
	if (delimiterLocation != std::string::npos) {
		while (delimiterLocation != std::string::npos) {
			symbols.push_back(arguments.substr(iterate, delimiterLocation - iterate));
			iterate = ++delimiterLocation;
			delimiterLocation = arguments.find(delimiter, delimiterLocation);
		}
		symbols.push_back(arguments.substr(iterate, delimiterLocation - iterate));
	}
	else
		symbols.push_back(arguments);
}

void Utility::split(std::string& source, std::string& remainder, const char delimiter) {
	std::string::size_type delimiterLocation = source.find(delimiter);
	std::string helper = source;
	source.assign(helper, 0, delimiterLocation);
	remainder.assign(helper, delimiterLocation + 1, std::string::npos);
}

void Utility::expunge(std::string& source, const char c) {
	std::string helper = source;
	source.erase();
	for (auto& a : source)
		if (a != c)
			source.push_back(a);
}

};
