#ifndef UTILITY_H_
#define UTILITY_H_

#include <string>
#include <vector>

namespace assembling {

class Utility final {
public:

	Utility() = delete;
	~Utility() = delete;
	Utility(Utility&) = delete;
	Utility(Utility&&) = delete;
	Utility& operator = (Utility&) = delete;

	static void split(std::string&, std::vector<std::string>&, const char);
	static void split(std::string&, std::string&, const char);
	static void trim(std::string&);
	static void expunge(std::string&, const char);

};
}

#endif
