#ifndef _STACKEXPRESSIONEVALUATOR_H_
#define _STACKEXPRESSIONEVALUATOR_H_
#include <stack>
#include <string>
#include <unordered_map>
#include <deque>
#include <vector>
#include <cstdint>
#include "Utility.h"

namespace assembling {

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

using EntryPriority = int8_t;
using StackPriority = int8_t;
using Rank = int8_t;

struct Token {
	char op;
	word number;
	bool representingOperator;

	bool isOperator() {
		return representingOperator;
	}

	bool isOperand() const {
		return !representingOperator;
	}

	Token(char c): op(c), number(0), representingOperator(true) {}
	Token(word n): op(0), number(n), representingOperator(false) {}
};

class StackExpressionEvaluator final {

private:

	static const std::unordered_map<byte, EntryPriority> allEntryPriorities;
	static const std::unordered_map<byte, StackPriority> allStackPriorities;
	static const std::unordered_map<byte, Rank> allRanks;
	static const std::vector<char> operators;

	static std::string expression;

	static std::deque<Token> tokenize(std::string);
	static std::deque<Token> generatePostfix(std::deque<Token>&);
	static word calculateValue(std::deque<Token>&);

public:

	static word evaluateExpression(std::string);
	static bool isOperand(const char);
	static bool isOperator(const char);



	StackExpressionEvaluator() = default;
	StackExpressionEvaluator(StackExpressionEvaluator&) = delete;
	StackExpressionEvaluator(StackExpressionEvaluator&&) = delete;
	StackExpressionEvaluator& operator = (StackExpressionEvaluator&) = delete;
	~StackExpressionEvaluator() = default;
};

} /* namespace assembling */

#endif
