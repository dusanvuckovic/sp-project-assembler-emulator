#ifndef OPERATIONCONSTRAINTS_H_
#define OPERATIONCONSTRAINTS_H_

#include <deque>
#include <list>
#include <algorithm>
#include <map>

namespace assembling {

enum class Type {
	Reg, Imm, Exp, Lab, NoT, Err
};

class OperationConstraints final {

public:
	OperationConstraints();
	std::map<uint32_t, std::map<uint32_t, std::deque<Type>>> allowed;

	bool isAllowed(uint32_t group, uint32_t number, Type type) {
		for (auto& typeMap: allowed.at(group).at(number))
			if (type == typeMap)
				return true;
		return false;
	}
};

} /* namespace assembling */

#endif /* OPERATIONCONSTRAINTS_H_ */
