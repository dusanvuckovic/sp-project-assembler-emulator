#ifndef _LABELDEFINEDTWICEEXCEPTION_H_
#define _LABELDEFINEDTWICEEXCEPTION_H_

#include <iostream>
#include "EndProgramException.h"

namespace assembling {


class EndProgramException;
class LabelDefinedTwiceException final: public EndProgramException {
public:
	LabelDefinedTwiceException(): EndProgramException() {
		this->errorMessage += "\nA label cannot be redefined!";
	}
};

} /* namespace assembling */

#endif
