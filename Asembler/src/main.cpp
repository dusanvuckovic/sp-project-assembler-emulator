#include <iostream>
#include "Assembler.h"
#include "EndProgramException.h"
#include "FileNotFoundException.h"
#include "NoSectionIndicatedException.h"
#include "NotInTextSectionException.h"
#include "Operation.h"
#include "Relocator.h"
#include "Section.h"
#include "Symbol.h"
#include <ctime>

#define __cplusplus 201402L

int main(int argc, char* argv[]) {
	try {
		std::string input { argv[1] };
		std::string output = input.substr(0, input.find_last_of('.')) + "_output.txt";
		assembling::Assembler a(input, output);
		a.preprocess();
		a.firstPass();
		a.resetLocationCounters();
		a.secondPass();
		a.writeOut();
		std::cout << "Assembler successfully assembled." << std::endl;
		return 0;
	} catch (assembling::EndProgramException &e) {
		std::cout << e;
		return -1;
	}
}
