#ifndef ASSEMBLER_H_
#define ASSEMBLER_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>
#include <algorithm>
#include <memory>
#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <cstdlib>
#include <cstdint>
#include <list>
#include <forward_list>
#include <regex>
#include "StackExpressionEvaluator.h"
#include "FileNotFoundException.h"
#include "NoSectionIndicatedException.h"
#include "Section.h"
#include "Operation.h"
#include "Utility.h"
#include "Relocator.h"
#include "Symbol.h"
#include "NotInTextSectionException.h"
#include "BSSSectionNotAllowedException.h"
#include "NotAValidExpressionException.h"
#include "WrongOperationCodeException.h"
#include "LabelDefinedTwiceException.h"
#include "RegexChecker.h"

namespace assembling {

class Relocator;
class Symbol;
class Operation;
class Operand;
class Compare;
class TextEditor;
class Writer;
class Section;
class RegexChecker;
class OperationBackpatch;
class OperationSet;

class Assembler final {

enum class Pass {NoPass, FirstPass, SecondPass};

private:
	std::string input, output;
	Pass currentPass;

public:
	std::unordered_map<std::string, Symbol*> symbols;
	std::map<std::string, Section*> sections;
	std::deque<Operation*> backpatches;
	std::list<Relocator*> relocations;
	std::vector<Operation*> operations;
	std::vector<std::string> lines;
	
private:

	std::string currentLine;
	std::string currentLabel;
	Section* currentSection;
	
	std::string beginning;
	std::vector<std::string> following;


	//operations
	bool setLabel(std::string&);
	void setDirective(std::string&);
	void setVariables(std::string&);
	void setOperation(std::string&);
	void setSection(std::string&);

	void addBytes(std::string&);
	void addGlobalElements(std::string&);
	void addExternElements(std::string&);
	void addWords(std::string&);
	void addSkip(std::string&);
	void addAlignment(std::string&);
	void addRelocations();
	void setRelocations(Operation&, Operand&);


public:

	Assembler(std::string, std::string); //input name, output name
	~Assembler() = default;

	void preprocess();
	void firstPass();
	void resetLocationCounters();
	void secondPass();
	void generate();
	void writeOut();

	std::pair<bool, word> getSymbolValue(const std::string);
};

}

#endif
