#test 1 za SP projekat, fajl 1

.data.d1
love: .word 21
lv: .byte 2
sym: .word 0x87654321

.text.t1
start: #int 3

.public subroutine2

ldc r2, 0x12345678
add r0, 1 #r0=1
add r1, r0 #r1=1
add r2, (98+8)*2 #r2=212
#add r3, 1000000 #r3=1000000, ne može opcode

not r1, r0 #r1=-2
sub r1, -3 #r1=1
mul r2, r1 #r2 = r2 * 1 = 212
mul r2, 4 #r2 = 212 * 4 = 848
div r2, 2 #r2 = 848/2 = 424
ldch r2, 0
ldcl r2, 2000
cmps r2, 2000
intne 3
strge r2, r1 #m[2000] = r1 = 1
ldrge r3, r2 #r3=m[2000] = 1
shl r2, r2, 1 #r2 = 4000
call subr
out r2, r1 #io[4000]=r1=1
in r4, r2 #r4 = io[4000]=1
mov r1, r0 #r1=1
shl r1, r1, 5 #r1=1<<5=32
shr r1, r0, 1 #r1=32>>1=16
ldcls r3, 93
ldch r4, 93
mov lr, 400
call subr2
ldc r0, sym

subr: 
push r2
pop r5
cmps r6, 0
add r6, 1
addne pc, 20
ret

.end