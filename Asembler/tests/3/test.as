#test 3 za SP projekat

.text.t1
start: 
ldc r10, 0x1000 #r10 const 0x1000
ldc r11, 0x1010 #r11 const 0x1010
ldc r12, 0x2000 #r12 const 0x2000
in r0, r11 #u r2 učitaj vrednost kontrolnog registra
cmps r0, 0x400 #uporedi bit 10, ima li materijala
callne start #vrti se dok nema
repeat: in r0, r10 #r0 = el
ldc r1, 0
cmp r0, 65
addeq r1, 1
cmp r0, 69
addeq r1, 1
cmp r0, 73
addeq r1, 1
cmp r0, 79
addeq r1, 1
cmp r0, 85
addeq r1, 1
cmp r0, 97
addeq r1, 1
cmp r0, 101
addeq r1, 1
cmp r0, 105
addeq r1, 1
cmp r0, 111
addeq r1, 1
cmp r0, 117
addeq r1, 1
cmp r1, 0
outeq r0, r12 #ako nije ijedan od ovih, ispiši
in r0, r11
cmp r0, 0x400
calleq repeat