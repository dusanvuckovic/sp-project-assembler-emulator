.text.t1
.extern twoConstant
start:
ldch r1, twoConstant
ldcl r1, twoConstant
ldch r2, 0
ldcl r2, 4
ldch r3, 0
ldcl r3, 1000
str r2, r3
ldch r2, 16384
ldcl r2, 0
mov r3, psw
or r3, r2
mov psw, r3
loop: call loop
.text.timer
mul r1, r1
cmp r1, 4096
addeq pc, 8
movs pc, lr
