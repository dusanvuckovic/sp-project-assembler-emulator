#include "StackExpressionEvaluator.h"
#include <cstdlib>

class StackExpressionErrorException;

namespace emulation {

const std::unordered_map<byte, EntryPriority> StackExpressionEvaluator::allEntryPriorities = {{'+', 2}, {'-', 2}, {'*', 3}, {'/', 3}, {'(', 6}, {')', 1}};
const std::unordered_map<byte, StackPriority> StackExpressionEvaluator::allStackPriorities = {{'+', 2}, {'-', 2}, {'*', 3}, {'/', 4}, {'(', 0}};
const std::unordered_map<byte, Rank> StackExpressionEvaluator::allRanks = {{'+', -1}, {'-', -1}, {'*', -1}, {'/', -1}};
const std::vector<char> StackExpressionEvaluator::operators = {'+', '-', '*', '/', '(', ')'};

bool StackExpressionEvaluator::isOperand(char candidate) {
	return !isOperator(candidate);
};

bool StackExpressionEvaluator::isOperator(char candidate) {
	for (auto c: operators)
		if (candidate == c)
			return true;
	return false;
};

std::deque<Token> StackExpressionEvaluator::tokenize(std::string inputString) {
	std::deque<Token> tokens;
	std::string helperString;
	for (auto c: inputString) {
		if (c == ' ')
			continue;
		else if (isOperator(c)) {
			if (!helperString.empty()) {
			    tokens.push_back(Token {std::stoi(helperString)});
				helperString.erase();
			}
			tokens.push_back(Token{c});
		}
		else
			helperString.push_back(c);
	}
	if (!helperString.empty())
		tokens.push_back(Token {std::stoi(helperString)});
	return tokens;
}

std::deque<Token> StackExpressionEvaluator::generatePostfix(std::deque<Token>& tokens) {
	Rank currentRank = 0;
	std::deque<Token> finalTokens;
	std::stack<Token> myStack;

	for (auto token: tokens) {
		if (token.isOperand()) {
			finalTokens.push_back(token);
			++currentRank;
		}
		else {
			while (myStack.size() && StackExpressionEvaluator::allEntryPriorities.at(token.op) <=
					StackExpressionEvaluator::allStackPriorities.at(myStack.top().op)) {
				Token t = myStack.top();
				myStack.pop();
				finalTokens.push_back(t);
				currentRank += allRanks.at(t.op);
				if (currentRank < 1) {
					; //throw exception
				}
			}
			if (token.op != ')')
				myStack.push(token);
			else {
				myStack.pop();
			}
		}
	}
	while (myStack.size()) {
		Token t = myStack.top();
		myStack.pop();
		//if (t.op != '(')
			finalTokens.push_back(t);
		currentRank += allRanks.at(t.op);
	}
	if (currentRank != 1) {
		; //baci izuzetak
	}
	return finalTokens;
}

word StackExpressionEvaluator::calculateValue(std::deque<Token>& tokens) {
	std::stack<Token> myStack;
	word result = 0;
	for (auto token: tokens) {
		if (token.isOperand())
			myStack.push(token);
		else {
			word operand2 = myStack.top().number;
			myStack.pop();
			word operand1 = myStack.top().number;
			myStack.pop();
			switch (token.op) {
				case '+':
					result = operand1 + operand2; break;
				case '-':
					result = operand1 - operand2; break;
				case '*':
					result = operand1 * operand2; break;
				case '/':
					result = operand1 / operand2; break;
			}
			myStack.push(result);
		}
	}
	result = myStack.top().number; myStack.pop();
	if (!myStack.empty())
		exit(-1);
	return result;
}

word StackExpressionEvaluator::evaluateExpression(std::string input) {
	auto tokens = tokenize(input);
	auto postfix = generatePostfix(tokens);
	return calculateValue(postfix);
}


}

