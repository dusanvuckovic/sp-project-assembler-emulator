#ifndef SECTION_H_
#define SECTION_H_

#include <vector>
#include <string>

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

namespace emulation {

class Section {

	private:
		uword sectionID;
		std::string sectionName;
		uword sectionLength;
		std::vector<byte> data;
		uword loadLocation;
	public:
		std::vector<byte> getData() {
			return data;
		}
		uword getLoadLocation() const {
			return loadLocation;
		}
		void setLoadLocation(uword location) {
			loadLocation = location;
		}
		const std::string& getSectionName() {
			return sectionName;
		}
		uword readOperation(uword address) {
			return data[address] << 24 | (data[address + 1] << 16)
				| (data[address + 2] << 8) | (data[address + 3]);
		}

		void writeOperation(uword address, uword op) {
			data[address] = static_cast<byte>((op & 0xff000000) >> 24);
			data[address + 1] = static_cast<byte>((op & 0x00ff0000) >> 16);
			data[address + 2] = static_cast<byte>((op & 0x0000ff00) >> 8);
			data[address + 3] = static_cast<byte>(op & 0x000000ff);

		}

		Section(std::vector<byte> data, uword sectionID, std::string sectionName, uword sectionLength): sectionID(sectionID),
			sectionName(sectionName), sectionLength(sectionLength), data(data), loadLocation(0) {}


};

} /* namespace emulation */

#endif /* SECTION_H_ */
