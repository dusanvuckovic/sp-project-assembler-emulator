#include "Emulator.h"
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char** argv) {

	std::vector<std::string> inputFiles;
	for (int i = 1; i < argc; i++)
		inputFiles.push_back(argv[i]);
	emulation::Emulator e(inputFiles);
	e.readFiles();
	e.link();
	e.load();
	e.runEmulator();
	std::cout << "Emulator successfully emulated.";
	return 0;
}


