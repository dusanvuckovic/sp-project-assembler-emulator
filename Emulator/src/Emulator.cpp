#include "Emulator.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include "Utility.h"
#include "Section.h"
#include "Symbol.h"
#include "Relocation.h"
#include "StackExpressionEvaluator.h"

namespace emulation {
class Utility;
class Section;
class Relocation;
class StackExpressionEvaluator;

Emulator::Emulator(std::vector<std::string>& input) {
	std::fstream copier(input[0], std::fstream::in);
	if (copier.fail())
		exit(-1); //	throw new FileNotFoundException();
	std::string helper;
	while (getline(copier, helper)) {
		Utility::trim(helper);
		if (helper != "")
			loadingScript.push_back(helper);
	}
	copier.close();
	for (uword i = 1; i < input.size(); i++) {
		std::fstream copier(input[i], std::fstream::in);
		std::vector<std::string> data;
		std::string helper;
		while (getline(copier, helper)) {
			Utility::trim(helper);
			if (helper != "")
				data.push_back(helper);
		}
		loadingFiles[input[i]] = data;
	}
}

Emulator::~Emulator() {
	for (auto r: relocations)
		delete r;
	for (auto& r: symbols)
		delete r.second;
	for (auto& r: sections)
		delete r.second;
}

void Emulator::loadSections(std::vector<std::string>::iterator& currentLine) {

	while (*currentLine != "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾") //dolazak do prvog
		++currentLine; //preskačemo
	++currentLine; //preskačemo "‾‾‾‾‾‾‾‾‾‾‾"

	while (*currentLine != "Tabela relokacija:") { //dok nije početak tabele relokacija

		//pročitaj podatke o sekciji
		int sectionID;
		std::string sectionName;
		uword sectionLength;

		std::string cur = *currentLine;
		std::stringstream metadata(cur);
		metadata >> sectionID;
		metadata >> sectionName;
		metadata >> sectionLength;

		std::vector<byte> bytes;
		std::vector<unsigned char> current;

		currentLine += 2;

		//dok nije početak sledeće sekcije
		//učitaj podatke u sekciju
		while (*currentLine != "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾") {
			std::string myLine = *currentLine;
			for (unsigned char c : myLine) {
				if (c != ' ' && c != '|') {
					current.push_back(c);
					continue;
				}
				byte b = 0;
				if (current.size() == 2) {
					b |= byteConversion.at(current[0]) << 4;
					b |= byteConversion.at(current[1]);
				} else {
					b |= byteConversion.at(current[0]);
				}
				current.clear();
				bytes.push_back(b);
			}
			++currentLine;
		}

		//dodaj sekciju u listu svih sekcija
		sections[sectionName] = new Section(bytes, sectionID, sectionName,
				sectionLength);
		//pređi na sledeću ili kraj
		++currentLine;
	}
}

void Emulator::setOperation(word value, word address) {
	memory[address + 3] = static_cast<byte>(value & 0x000000ff);
	memory[address + 2] = static_cast<byte>((value & 0x0000ff00) >> 8);
	memory[address + 1] = static_cast<byte>((value & 0x00ff0000) >> 16);
	memory[address] = static_cast<byte>((value & 0xff000000) >> 24);
}

word Emulator::getOperation(word address) {
	if (!memory.count(address) || !memory.count(address+1) || !memory.count(address+2) || !memory.count(address+3))
			exit(-2);
	return (memory[address] << 24 | (memory[address + 1] << 16)
		| (memory[address + 2] << 8) | (memory[address + 3]));
}

void Emulator::loadSymbolTable(
		std::vector<std::string>::iterator& currentLine) {
	while (*currentLine
			!= "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾")
		++currentLine; //dolazak do početka
	++currentLine; //prelaz na prvi element
	while (*currentLine != "Sekcije:") {
		std::string myLine = *currentLine;
		uword symbolID;
		std::string symbolName, sectionName;
		uword offset;
		std::string isLocal, isDefined;
		bool local = false, defined = false;
		word value;

		std::stringstream line(*currentLine);
		line >> symbolID;
		line >> symbolName;
		line >> sectionName;
		line >> offset;
		line >> isLocal;
		line >> isDefined;
		line >> value;

		if (isLocal == "true")
			local = true;
		if (isDefined == "true")
			defined = true;

		symbols[symbolName] = new Symbol(symbolID, symbolName, sectionName,
				offset, local, defined, value);

		++currentLine;
	}
}

void Emulator::loadRelocations(std::vector<std::string>& currentFile,
		std::vector<std::string>::iterator& currentLine) {
	while (true) {
		std::string myLine = *currentLine;
		if (myLine
				== "|ID|Ime simbola|ID sekcije|Ime sekcije|Pomeraj|Relok. tip|") {
			currentLine += 2;
			break;
		} else
			++currentLine;
	}
	while (true) {
		if (currentLine == currentFile.end())
			return;

		uword relocationID;
		std::string symbolName;
		uword sectionID;
		std::string sectionName;
		uword offset;
		std::string relocationType;

		std::stringstream line(*currentLine);
		line >> relocationID;
		line >> symbolName;
		line >> sectionID;
		line >> sectionName;
		line >> offset;
		line >> relocationType;

		relocations.push_back(
				new Relocation(relocationID, symbolName, sectionID, sectionName,
						offset, relocationType));

		currentLine += 2;
	}
}


void Emulator::readFiles() {
	for (auto& file : loadingFiles) {
		auto &currentFileLines = file.second;
		auto currentLine = currentFileLines.begin();
		while (currentLine != currentFileLines.end()) {
			std::string myLine = *currentLine;
			if (myLine == sectionHeader)
				loadSections(currentLine);
			else if (myLine == symbolTableHeader)
				loadSymbolTable(currentLine);
			else if (myLine == relocationTableHeader)
				loadRelocations(currentFileLines, currentLine);
			else
				++currentLine;
		}
	}
}

void Emulator::link() {
	std::vector<std::string> newLoadingScript;
	for (auto line: loadingScript) {
		if (line.substr(0, 2) == ".=")
			newLoadingScript.push_back(line);
		else if (line.find("=") != std::string::npos) {
			auto delimiter = line.find("=");
			std::string symbolName = line.substr(0, delimiter);
			std::string myValue = line.substr(delimiter+1);
			Utility::trim(symbolName);
			Utility::trim(myValue);
			word valueWord = StackExpressionEvaluator::evaluateExpression(myValue);
			if (symbols.count(symbolName))
				symbols[symbolName]->setValue(valueWord);
			else
				symbols[symbolName] = new Symbol(0, symbolName, "", 0, false, true, valueWord);
		}
		else
			newLoadingScript.push_back(line);
	}
	loadingScript = newLoadingScript;

	std::vector<Relocation*> remainingRelocations;
	for (auto r: relocations) {
		auto type = r->getRelocationType();
		if (type != RelocationType::R_HYPO_LINK_LDCH && type != RelocationType::R_HYPO_LINK_LDCL)
			remainingRelocations.push_back(r);
		else {
			std::string symbolName = r->getSymbolName();
			Section *s = sections[r->getSectionName()];
			uword value = symbols[symbolName]->getValue();
			uword address = r->getOffset();
			uword op = s->readOperation(address);
			if (type == RelocationType::R_HYPO_LINK_LDCH)
				value = (value >> 16) & 0xffff;
			else
				value &= 0xffff;
			op |= value;
			s->writeOperation(address, op);
			delete r;
		}
	}
	relocations = remainingRelocations;
}

void Emulator::load() {
	uword currentMemoryPosition = 16*4;
	std::vector<std::string> loadedSections;
	for (auto line: loadingScript) {

		if (line.find("ALIGN(") != std::string::npos) {
			auto start = line.find("ALIGN(");
			auto end = line.find(")");
			auto remainString = line.substr(0, start) + line.substr(end+1);
			auto helperString = line.substr(start+6, end-start-1);
			std::stringstream alignstring(helperString);
			std::string first, second;
			word num1, num2;
			std::getline(alignstring, first, ',');
			std::getline(alignstring, second, ')');
			num1 = std::stoi(first);
			num2 = std::stoi(second);
			while (num1 % num2)
				++num1;
			remainString += std::to_string(num1);
			line = remainString;
		}
		//ako je promena pozicije
		if (line.substr(0, 2) == ".=") {
			uword nextPosition;
			line = line.substr(2);
			Utility::trim(line);
			nextPosition = StackExpressionEvaluator::evaluateExpression(line);
			//prijavi grešku
			if (nextPosition < currentMemoryPosition) {
				exit(-3);
			}
			currentMemoryPosition = nextPosition;
		}
		//učitavamo sekciju
		else {
			uword starting = currentMemoryPosition, ending;
			if (!sections.count(line)) //ako nema takve sekcije
				exit(-4);
			Section *s = sections[line];
			std::string sectionName = s->getSectionName();
			if (std::find(loadedSections.begin(), loadedSections.end(), sectionName) != loadedSections.end())
				exit(-3); //ponovljena sekcija
			else
				loadedSections.push_back(sectionName);
			s->setLoadLocation(currentMemoryPosition);
			std::vector<byte> sectionData = s->getData();
			for (byte b: sectionData)
				memory[currentMemoryPosition++] = b;
			ending = currentMemoryPosition;
			if (line.find(".text") != std::string::npos)
				sectionBoundaries.push_back(std::pair<uword, uword>(starting, ending));
		}
	}

	//dodaj relokacije za labele
	std::vector<Relocation*> newRelocations;
	for (auto r: relocations) {
		if (r->getRelocationType() == RelocationType::R_HYPO_LOAD_LABEL) {
			Symbol *label = symbols[r->getSymbolName()];
			Section *sectionOfLabel = sections[label->getSectionName()];
			label->setOffset(label->getOffset() + sectionOfLabel->getLoadLocation());
			delete r;
		}
		else
			newRelocations.push_back(r);
	}
	relocations = newRelocations;
	for (auto r: relocations) {
		Symbol *label = symbols[r->getSymbolName()];
		Section *sectionOfPc = sections[r->getSectionName()];

		uword labelPosition = label->getOffset();
		uword pcPosition = sectionOfPc->getLoadLocation() + r->getOffset() + 4;

		uword address = pcPosition - 4;
		word offset = labelPosition - pcPosition;
		offset &= 0x7ffff;
		uword op = getOperation(address);
		op &= ~(0x7ffff);

		if ((uword)offset <= (uword)(1<<19)) {
			op |= offset;
			setOperation(op, address);
		}

		else
			exit(-2);
	}
}

void Emulator::checkForSymbols() {
	for (auto& symbol: symbols) {
		if (!symbol.second->isDefined())
			exit(-3); //nisu svi simboli definisani!
	}
}


void Emulator::runEmulator() {
	if (!symbols.count("start"))
		exit(-6);
	Symbol *s = symbols.at("start");
	word startAddress = s->getOffset();
	Operation o(memory, startAddress, sectionBoundaries);
	o.run();
}

} /* namespace emulation */

