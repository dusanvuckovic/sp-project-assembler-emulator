#ifndef RELOCATION_H_
#define RELOCATION_H_

namespace emulation {

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

enum class RelocationType {
	R_HYPO_LOAD_LABEL, R_HYPO_LINK_CALL, R_HYPO_LINK_LDCH, R_HYPO_LINK_LDCL
};

const std::map<std::string, RelocationType> relocationTypes {
	{"R_HYPO_LOAD_LABEL", RelocationType::R_HYPO_LOAD_LABEL},
	{"R_HYPO_LINK_CALL", RelocationType::R_HYPO_LINK_CALL},
	{"R_HYPO_LINK_LDCH", RelocationType::R_HYPO_LINK_LDCH},
	{"R_HYPO_LINK_LDCL", RelocationType::R_HYPO_LINK_LDCL}
};

class Relocation {

private:

	uword relocationID;
	std::string symbolName;
	uword sectionID;
	std::string sectionName;
	uword offset;
	RelocationType relocationType;

public:
	const std::string& getSymbolName() const {
		return symbolName;
	}
	const std::string& getSectionName() const {
		return sectionName;
	}
	const RelocationType& getRelocationType() const {
		return relocationType;
	}

	const uword getOffset() const {
		return offset;
	}
	Relocation(uword relocationID, std::string symbolName, uword sectionID, std::string sectionName, uword offset,
			std::string relocationType): relocationID(relocationID), symbolName(symbolName), sectionID(sectionID),
			sectionName(sectionName), offset(offset) {
		this->relocationType = relocationTypes.at(relocationType);

	}
};




} /* namespace emulation */





#endif /* RELOCATION_H_ */
