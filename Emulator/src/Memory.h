#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdint.h>
#include <map>

namespace emulation {


using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

class Memory {

	private:
		uword maximumSize;
		std::map<uword, byte> memory;
	public:
		Memory(uword maximumSize): maximumSize(maximumSize) {}

		void loadMemory(std::map<uword, byte> memory) {
			this->memory = memory;
		}
		word getOperation(word);
		word getMem(word);
		void setMem(word, word);
		byte getByte(word);
		void setByte(word, byte);

};

} /* namespace emulation */

#endif /* MEMORY_H_ */
