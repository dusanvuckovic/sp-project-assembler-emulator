#ifndef UTILITY_H_
#define UTILITY_H_

#include <string>

namespace emulation {

class Utility {
public:

	static int signItUp(int, int);
	static void trim(std::string&);

	Utility() = delete;
	virtual ~Utility() = delete;
};

}
#endif
