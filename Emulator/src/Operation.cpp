#include "Operation.h"
#include "Emulator.h"
#include "Utility.h"
#include "Timer.h"
#include "CharacterReader.h"
#include <vector>

namespace emulation {

	Register Operation::r0, Operation::r1, Operation::r2, Operation::r3, Operation::r4, Operation::r5, Operation::r6,
	Operation::r7, Operation::r8, Operation::r9, Operation::r10, Operation::r11, Operation::r12,
	Operation::r13, Operation::r14, Operation::r15, Operation::pc, Operation::lr,
	Operation::sp, Operation::psw;

	Register Operation::pc_reset, Operation::sp_reset;
	bool Operation::periodicFlag = false;
	Timer Operation::timer;
	word Operation::IR;
	Memory Operation::memory(429496729); //2^32
	IO Operation::io(65536); //2^16
	OperationAttributes Operation::currentOperation;
	bool Operation::keyboardFlag;
	std::unordered_map<uword, std::string> Operation::myOperationCodes;
	word Operation::CLK;
	Time Operation::startTime, Operation::endTime;
	std::vector<std::pair<uword, uword>> Operation::textBounds;
	CharacterReader Operation::reader;

	std::map<int, Register&> Operation::registers {
		{0, r0}, {1, r1}, {2, r2}, {3, r3}, {4, r4},
		{5, r5}, {6, r6}, {7, r7}, {8, r8}, {9, r9},
		{10, r10}, {11, r11}, {12, r12}, {13, r13}, {14, r14},
		{15, r15}, {16, pc}, {17, lr}, {18, sp}, {19, psw}
	};

	std::map<OperationCode, OperationBody> Operation::myOperations {
		{OperationCode::Int, doInt}, {OperationCode::Add, doAdd}, {OperationCode::Sub, doSub},
		{OperationCode::Mul, doMul}, {OperationCode::Div, doDiv}, {OperationCode::Cmp, doCmp},
		{OperationCode::And, doAnd}, {OperationCode::Or, doOr}, {OperationCode::Not, doNot},
		{OperationCode::Test, doTest}, {OperationCode::LdrStr, doLdrStr},
		{OperationCode::Call, doCall}, {OperationCode::InOut, doInOut},
		{OperationCode::MovShrShl, doMovShrShl}, {OperationCode::Ldchl, doLdchl}
	};

bool Operation::areInterruptsMasked() {
	return psw & 0x80000000;
}

bool Operation::isTimedInterruptSet() {
	return psw & 0x40000000;
}

void Operation::clearPSW() {
	psw &= ~0xc0000000;
}

bool Operation::isZSet() {
	return psw & 0x08;
}
bool Operation::isOSet() {
	return psw & 0x04;
}
bool Operation::isCSet() {
	return psw & 0x02;
}
bool Operation::isNSet() {
	return psw & 0x01;
}

void Operation::setN() {
	psw |= 0x1;
}
void Operation::setC() {
	psw |= 0x2;
}
void Operation::setO() {
	psw |= 0x4;
}
void Operation::setZ() {
	psw |= 0x8;
}

void Operation::clearN() {
	psw &= ~(0x1);
}
void Operation::clearC() {
	psw &= ~(0x2);
}
void Operation::clearO() {
	psw &= ~(0x4);
}
void Operation::clearZ() {
	psw &= ~(0x8);
}


void Operation::resetRoutine() {
	for (auto& r: registers)
		r.second = 0;
	pc = pc_reset;
	sp = sp_reset;
	CLK = 0;
	IR = 0;
	timer.reset();
}

void Operation::periodicRoutine() {
	periodicFlag = true;
	doInt();
	periodicFlag = false;
}

void Operation::skipRoutine() {
	++pc;
}

void Operation::generatePSWZN(word result) {
	if (currentOperation.opFlags == OperationFlags::NoFlagUpdate)
		return;
	if (!result)
		setZ();
	else
		clearZ();
	if (result < 0)
		setN();
	else
		clearN();
}

void Operation::generatePSWZCN(word result, word firstArgument, word secondArgument) {
	if (currentOperation.opFlags == OperationFlags::NoFlagUpdate)
		return;
	generatePSWZN(result);
	if (carry(firstArgument, secondArgument))
		setC();
	else
		clearC();
}

void Operation::generatePSWZOCN(word result, word firstArgument, word secondArgument) {
	if (currentOperation.opFlags == OperationFlags::NoFlagUpdate)
		return;
	generatePSWZCN(result, firstArgument, secondArgument);
	if (overflow(firstArgument, secondArgument))
		setO();
	else
		clearO();
}

word Operation::signExt(word myWord, int numberOfBits) {
	if (!(myWord & (1 << (numberOfBits-1) )))
		return myWord;
	word mask = (1 << numberOfBits) - 1;
	mask = ~mask;
	myWord |= mask;
	return myWord;
}

void Operation::pushStack(word value) {
	sp += 4;
	memory.setMem(value, sp);
}
word Operation::popStack() {
	word result = memory.getMem(sp);
	sp += 4;
	return result;
}

bool Operation::overflow(word a, word b) {
	word result = a + b;
	if (a > 0 && b > 0 && result < 0)
		return true;
	if (a < 0 && b < 0 && result > 0)
		return true;
	return false;
}

bool Operation::carry(word a, word b) {
	uword ua = static_cast<uword>(a);
	uword ub = static_cast<uword>(b);
	uword result = ua + ub;

	if (result < ua || result < ub)
		return true;
	return false;
}

bool Operation::outOfBounds(word pc) {
	for (auto& boundsPairs: textBounds)
		if ((uword)pc > boundsPairs.first && (uword)pc < boundsPairs.second)
			return false;
	return true;
}

} /* namespace emulation */
