#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>

namespace emulation {

class Timer {

private:
	long long oldSeconds = 0;
public:

bool getElapsedTime() {
    struct timeval te;
    gettimeofday(&te, nullptr);
    if (te.tv_sec - oldSeconds >= 1) {
    	oldSeconds = te.tv_sec;
    	return true;
    }
    return false;
}

void reset() {
	oldSeconds = 0;
}

};

} /* namespace emulation */

#endif /* TIMER_H_ */
