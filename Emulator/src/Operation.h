#ifndef OPERATION_H_
#define OPERATION_H_

#include "Utility.h"
#include "Emulator.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>
#include <bitset>
#include <ctime>
#include <cstdint>
#include <map>
#include "Memory.h"
#include "IO.h"
#include "Timer.h"
#include "CharacterReader.h"

namespace emulation {

using Mask = uint32_t;

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

using MemoryAddress = uint32_t;
using Register = int32_t;
using Time = time_t;

using OperationBody = void(*) ();

enum class OperationCode {
	Int, Add, Sub, Mul, Div, Cmp, And, Or, Not, Test, LdrStr, Call, InOut, MovShrShl, Ldchl, Err
};

enum class OperationFlags {
	NoFlagUpdate, FlagUpdate, Err
};

enum class OperationExecution {
	Eq, Ne, Gt, Ge, Lt, Le, Reserved, Al, Err
};

struct OperationAttributes {
	OperationCode opCode = OperationCode::Err;
	OperationFlags opFlags = OperationFlags::Err;
	OperationExecution opExec = OperationExecution::Err;
};

const std::map<uword, OperationExecution> bitset_to_conditional {
		{0, OperationExecution::Eq}, {1, OperationExecution::Ne},
		{2, OperationExecution::Gt}, {3, OperationExecution::Ge},
		{4, OperationExecution::Lt}, {5, OperationExecution::Le},
		{7, OperationExecution::Al}
};

const std::map<uword, OperationCode> bitset_to_opcode = {
		{0, OperationCode::Int}, {1, OperationCode::Add}, {2, OperationCode::Sub},
		{3, OperationCode::Mul}, {4, OperationCode::Div}, {5, OperationCode::Cmp},
		{6, OperationCode::And}, {7, OperationCode::Or}, {8, OperationCode::Not,},
		{9, OperationCode::Test}, {10, OperationCode::LdrStr},
		{12, OperationCode::Call}, {13, OperationCode::InOut},
		{14, OperationCode::MovShrShl}, {15, OperationCode::Ldchl}
};


class Operation {

private:

	static OperationAttributes currentOperation;

	static Register r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11,
		r12, r13, r14, r15, pc, lr, sp, psw;
	static std::map<int, Register&> registers;

	static Register pc_reset, sp_reset;
	static bool periodicFlag;

	static Register& registerFile(int registerNumber) {
		if (registerNumber < 0 || registerNumber > 19)
			exit(-2);
		return registers.at(registerNumber);
	}

	static Timer timer;
	static Time startTime, endTime;
	static word CLK;
	static word IR;
	static Memory memory;
	static IO io;
	static CharacterReader reader;
	static std::vector<std::pair<uword, uword>> textBounds;

	static bool areInterruptsMasked();
	static bool isTimedInterruptSet();
	static void clearPSW();
	static bool isZSet();
	static bool isOSet();
	static bool isCSet();
	static bool isNSet();

	static void setZ();
	static void setO();
	static void setC();
	static void setN();

	static void clearZ();
	static void clearO();
	static void clearC();
	static void clearN();

	static bool isEq();
	static bool isNe();
	static bool isGt();
	static bool isLt();
	static bool isLe();

	static void generatePSWZN(word);
	static void generatePSWZCN(word, word, word);
	static void generatePSWZOCN(word, word, word);

	static void pushStack(word value);
	static word popStack();

	static word signExt(word, int);
	static bool keyboardFlag;
	static bool overflow (word, word);
	static bool carry (word, word);
	static bool outOfBounds(word);

	static void resetRoutine();
	static void periodicRoutine();
	static void skipRoutine();


public:

	static std::map<OperationCode, OperationBody> myOperations;
	static std::unordered_map<uword, std::string> myOperationCodes;

	explicit Operation(std::map<uword, byte> memoryData, word startAddress, std::vector<std::pair<uword, uword>> bounds) {
		memory.loadMemory(memoryData);
		textBounds = bounds;
		for (auto& reg: registers)
			reg.second = 0;
		pc = pc_reset = startAddress;
		sp = sp_reset = 8000;

		IR = 0;
		CLK = 0;
		timer.getElapsedTime();
	}

	static bool isOutOfBoundaries() {
		bool isValid = false;
		for (auto& boundaries: textBounds)
			if (boundaries.first <= (uword)pc && (uword)pc <= boundaries.second) {
				isValid = true;
				break;
			}
		return !isValid;
	}

	static bool shouldExecuteOperation() {
		switch (currentOperation.opExec) {
			case OperationExecution::Al: return true;
			case OperationExecution::Eq: return isZSet();
			case OperationExecution::Ne: return !isZSet();
			case OperationExecution::Gt: return !(isZSet() || isNSet());
			case OperationExecution::Ge: return !isNSet();
			case OperationExecution::Lt: return isNSet();
			case OperationExecution::Le: return isZSet() || isNSet();

			case OperationExecution::Reserved: exit(-1);
			case OperationExecution::Err: exit(-1);
		}
		return true;
	}


	static void run() {
		//time(&startTime);
		while (true) {
			reader.read_line();
			if (isOutOfBoundaries())
				return;
			IR = memory.getOperation(pc);
			pc += 4;
			currentOperation = getOperation();
			if (shouldExecuteOperation())
				myOperations[currentOperation.opCode]();
			++CLK;
			if (timer.getElapsedTime() >= 1 && isTimedInterruptSet())
				periodicRoutine();
		}
	}

	static OperationAttributes getOperation () {
		OperationAttributes oa;

		int exec = getBits(31, 3);
		bool flags = isBitSet(28);
		int operation = getBits(27, 4);

		oa.opCode = bitset_to_opcode.at(operation);
		oa.opFlags = (flags) ? OperationFlags::FlagUpdate : OperationFlags::NoFlagUpdate;
		oa.opExec = bitset_to_conditional.at(exec);

		return oa;
	}

	static bool isBitSet(int startingNumber, Register& R = IR) {
		word mask = 1 << startingNumber;
		return R & mask;
	}

	static uword getBits(int startingNumber, int numberOfBits, Register& R = IR) {
		uword mask = (1 << numberOfBits) - 1;
		uword result = R;
		result >>= (startingNumber - numberOfBits + 1);
		result &= mask;
		return result;
	}

	static uword getBitsToEnd(int startingNumber, Register& R = IR) {
		uword numberOfBits = startingNumber + 1;
		uword mask = (1 << numberOfBits) - 1;
		return R & mask;
	}

	static void doInt() {
		lr = pc;
		pushStack(psw);
		clearPSW();
		uword intNumber = getBits(23, 4);
		if (!intNumber)
			resetRoutine();
		else if (intNumber == 2)
			skipRoutine();
		//else if (intNumber == 3)
			//buttonRoutine();
		else {
			if (periodicFlag)
				intNumber = 1;
			intNumber <<= 2;
			pc = memory.getMem(intNumber);
		}
	}

	static void doAdd() {
		word& myRegister = registerFile(getBits(23, 5));
		word firstArgument = myRegister, secondArgument;
		if (!isBitSet(18))
			secondArgument = registerFile(getBits(17, 5));
		else
			secondArgument = getBitsToEnd(17);
		myRegister += secondArgument;
		generatePSWZOCN(myRegister, firstArgument, secondArgument);
	}

	static void doSub() {
		word& myRegister = registerFile(getBits(23, 5));
		word firstArgument = myRegister, secondArgument;
		if (!isBitSet(18))
			secondArgument = registerFile(getBits(17, 5));
		else
			secondArgument = signExt(getBitsToEnd(17), 17);
		myRegister -= secondArgument;
		generatePSWZOCN(myRegister, firstArgument, -secondArgument);
	}
	static void doMul() {
		word& myRegister = registerFile(getBits(23, 5));
		word secondArgument;
		if (!isBitSet(18))
			secondArgument = registerFile(getBits(17, 5));
		else
			secondArgument = getBitsToEnd(17);
		myRegister *= secondArgument;
		generatePSWZN(myRegister);
	}
	static void doDiv() {
		word& myRegister = registerFile(getBits(23, 5));
		word secondArgument;
		if (!isBitSet(18))
			secondArgument = registerFile(getBits(17, 5));
		else
			secondArgument = getBitsToEnd(17);
		myRegister /= secondArgument;
		generatePSWZN(myRegister);
	}
	static void doCmp() {
		word firstArgument = registerFile(getBits(23, 5)), secondArgument;
		word result = firstArgument;
		if (!isBitSet(18))
			secondArgument = registerFile(getBits(17, 5));
		else
			secondArgument = getBitsToEnd(17);
		result -= secondArgument;
		generatePSWZOCN(result, firstArgument, -secondArgument);
	}
	static void doAnd() {
		word& myRegister = registerFile(getBits(23, 5));
		word secondArgument = registerFile(getBits(18, 5));
		myRegister &= secondArgument;
		generatePSWZN(myRegister);
	}
	static void doOr() {
		word& myRegister = registerFile(getBits(23, 5));
		word secondArgument = registerFile(getBits(18, 5));
		myRegister |= secondArgument;
		generatePSWZN(myRegister);
	}
	static void doNot() {
		word& myRegister = registerFile(getBits(23, 5));
		word secondArgument = registerFile(getBits(18, 5));
		myRegister = ~secondArgument;
		generatePSWZN(myRegister);
	}
	static void doTest() {
		word firstArgument = registerFile(getBits(23, 5));
		word secondArgument = registerFile(getBits(18, 5));
		firstArgument &= secondArgument;
		generatePSWZN(firstArgument);
	}

	static void doLdrStr() {
		word& reg1 = registerFile(getBits(23, 5));
		word& reg2 = registerFile(getBits(18, 5));
		word f = getBits(13, 3);
		bool ls = isBitSet(10);
		word immediate = signExt(getBitsToEnd(9), 9);

		if (f == 4)
			reg1 += 4;
		else if (f == 5)
			reg1 -= 4;
		if (ls)
			reg1 = memory.getMem(reg2 + immediate);
		else
			memory.setMem(reg2, reg1 + immediate);
		if (f == 2)
			reg2 += 4;
		else if (f == 3)
			reg2 -= 4;
	}
	static void doCall() {
		word& myRegister = registerFile(getBits(23, 5));
		word immediate = signExt(getBitsToEnd(18), 18);
		lr = pc;
		pc = myRegister + immediate;
	}
	static void doInOut() {
		word& dst = registerFile(getBits(23, 4));
		word src = registerFile(getBits(19, 4));
		bool isInput = isBitSet(15);
		if (src == 0x1000)
			dst = reader.readToRegister();
		else if (src == 0x1010)
			dst = reader.keyboardStatusWord();
		else if (src == 0x2000)
			reader.writeToOutput(dst);
		else if (isInput)
			dst = io.getIO(src);
		else
			io.setIO(src, dst);
	}
	static void doMovShrShl() {
		bool isPC = (getBits(23, 5) == 16);
		word& dst = registerFile(getBits(23, 5));
		word src = registerFile(getBits(18, 5));
		uword imm = getBits(13, 5);
		bool isL = isBitSet(8);

		if (isL)
			src <<= imm;
		else
			src >>= imm;

		dst = src;

		if (isPC && currentOperation.opFlags == OperationFlags::FlagUpdate)
			psw = popStack();
	}
	static void doLdchl() {
		word& dst = registerFile(getBits(23, 4));
		bool isH = isBitSet(19);
		word c = getBitsToEnd(15);
		if (isH) {
			dst &= 0x0000ffff;
			c <<= 16;
		}
		else
			dst &= 0xffff0000;
		dst |= c;
	}

};

} /* namespace emulation */

#endif /* OPERATION_H_ */
