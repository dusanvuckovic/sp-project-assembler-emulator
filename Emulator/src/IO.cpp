#include "IO.h"

namespace emulation {

byte IO::getByteIO(word address) {
	if (!io.count(address))
		exit(-2);
	return io[address];
}

void IO::setByteIO(word address, byte data) {
	io[address] = data;
}

word IO::getIO(word address) {
	if (!io.count(address) || !io.count(address+1) || !io.count(address+2) || !io.count(address+3))
		exit(-2);
	return (io[address] | (io[address + 1] << 8)
			| (io[address + 2] << 16) | (io[address + 3] << 24));
}

void IO::setIO(word value, word address) {
	io[address] = static_cast<byte>(value & 0x000000ff);
	io[address + 1] = static_cast<byte>((value & 0x0000ff00) >> 8);
	io[address + 2] = static_cast<byte>((value & 0x00ff0000) >> 16);
	io[address + 3] = static_cast<byte>((value & 0xff000000) >> 24);
}

} /* namespace emulation */
