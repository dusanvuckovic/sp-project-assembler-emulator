#ifndef SYMBOL_H_
#define SYMBOL_H_

namespace emulation {

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

class Symbol {

	private:
		uword ID;
		std::string symbolName;
		std::string sectionName;
		uword offset;
		bool local;
		bool defined;
		word value;
	public:
		word getValue() const {
			return value;
		}
		void setValue(word value) {
			this->value = value;
		}

		std::string getSectionName() const {
			return sectionName;
		}

		bool isDefined() const {
			return defined;
		}

		uword getOffset() const {
			return offset;
		}

		void setOffset(uword offset) {
			this->offset = offset;
		}

		Symbol(uword ID, std::string symbolName, std::string sectionName,
				uword offset, bool local, bool defined, word value):
			ID(ID), symbolName(symbolName), sectionName(sectionName),
			offset(offset), local(local), defined(defined), value(value) {}
};


} /* namespace emulation */


#endif /* SYMBOL_H_ */
