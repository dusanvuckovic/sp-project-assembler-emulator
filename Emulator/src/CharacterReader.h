#ifndef CHARACTERREADER_H_
#define CHARACTERREADER_H_

#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>

namespace emulation {

class CharacterReader {
private:
	std::string myBuffer;

private:
    struct termios t;

public:

    CharacterReader() {
    	int flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    	fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
    }

    ~CharacterReader() {
    	int flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    	flags &= (~O_NONBLOCK);
		fcntl(STDIN_FILENO, F_SETFL, flags);
    }

	void read_line() {
		char buffer[1024];
		int count = read(STDIN_FILENO, buffer, 1024);
		if (count < 0 && errno == EAGAIN)
			return;
		else
			for (int i = 0; i < count-1; i++) {
				char c = buffer[i];
				myBuffer.push_back(c);
			}
	}

	char readToRegister() {
		if (myBuffer.empty())
			exit(-3);
		char c = myBuffer[0];
		myBuffer = myBuffer.substr(1);
		return c;
	}

	word keyboardStatusWord() {
		if (myBuffer.empty())
			return 0;
		else
			return 1 << 10;
	}

	void writeToOutput(word wr) {
		char c = static_cast<char>(wr);
		std::cout << c;
	}
};

} /* namespace emulation */

#endif /* CHARACTERREADER_H_ */
