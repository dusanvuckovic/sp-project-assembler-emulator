#include "Memory.h"
namespace emulation {

byte Memory::getByte(word address) {
	if (!memory.count(address))
		exit(-2);
	return memory[address];
}

void Memory::setByte(word address, byte data) {
	memory[address] = data;
}

word Memory::getMem(word address) {
	if (!memory.count(address) || !memory.count(address+1) || !memory.count(address+2) || !memory.count(address+3))
			exit(-2);
		return (memory[address] | (memory[address + 1] << 8)
				| (memory[address + 2] << 16) | (memory[address + 3] << 24));
}

void Memory::setMem(word value, word address) {
	memory[address] = static_cast<byte>(value & 0x000000ff);
	memory[address + 1] = static_cast<byte>((value & 0x0000ff00) >> 8);
	memory[address + 2] = static_cast<byte>((value & 0x00ff0000) >> 16);
	memory[address + 3] = static_cast<byte>((value & 0xff000000) >> 24);
}

word Memory::getOperation(word address) {
	if (!memory.count(address) || !memory.count(address+1) || !memory.count(address+2) || !memory.count(address+3))
			exit(-2);
	return (memory[address+3] | (memory[address + 2] << 8)
		| (memory[address+1] << 16) | (memory[address] << 24));
}

} /* namespace emulation */
