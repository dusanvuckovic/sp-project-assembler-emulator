#ifndef EMULATOR_H_
#define EMULATOR_H_

#include <cstdint>
#include <cstdlib>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <functional>
#include <iterator>
#include <string>
#include <map>
#include <iostream>
#include "Operation.h"
#include "Utility.h"

namespace emulation {

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

class Operation;
class Utility;
class Section;
class Symbol;
class Relocation;

const std::string symbolTableHeader = "Tabela simbola:";
const std::string sectionHeader = "Sekcije:";
const std::string relocationTableHeader = "Tabela relokacija:";

static const std::map<char, word> byteConversion {
		{'0', 0},
		{'1', 1},
		{'2', 2},
		{'3', 3},
		{'4', 4},
		{'5', 5},
		{'6', 6},
		{'7', 7},
		{'8', 8},
		{'9', 9},
		{'a', 10},
		{'b', 11},
		{'c', 12},
		{'d', 13},
		{'e', 14},
		{'f', 15}
};

class Emulator final {

private:

	std::vector<std::string> loadingScript; //array[commands]
	std::map<std::string, uword> loadingPositions; //filename->loadingPosition
	std::map<std::string, std::vector<std::string>> loadingFiles; // filename->array[lines]

	std::map<std::string, Symbol*> symbols;
	std::map<std::string, Section*> sections;
	std::vector<std::pair<uword, uword>> sectionBoundaries;
	std::vector<Relocation*> relocations;

	std::map<uword, byte> memory;
	static Operation o;

	void loadSymbolTable(std::vector<std::string>::iterator&);
	void loadSections(std::vector<std::string>::iterator&);
	void loadRelocations(std::vector<std::string>&, std::vector<std::string>::iterator&);

	word getOperation(word address);
	void setOperation(word value, word address);

public:
	Emulator(std::vector<std::string>& loadFiles);
	~Emulator();

	void readFiles();
	void processFiles();
	void link();
	void load();
	void checkForSymbols();
	void runEmulator();

};

} /* namespace emulation */

#endif /* EMULATOR_H_ */
