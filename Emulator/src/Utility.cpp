#include "Utility.h"

namespace emulation {

int Utility::signItUp(int value, int numberOfItsBits) {
	char sign = value & numberOfItsBits;
	for (int i = 0; i < 32-numberOfItsBits; i++)
		value |= (sign >> (32-i));
	return value;
}

void Utility::trim(std::string& currentLine) {
	if (currentLine.find("@") != std::string::npos)
		currentLine = currentLine.substr(0, currentLine.find("@"));
	if (currentLine.find("#") != std::string::npos)
			currentLine = currentLine.substr(0, currentLine.find("#"));
	while (currentLine.front() == '\t' || currentLine.front() == ' ')
		currentLine.erase(0, 1); //erase it
	while (currentLine.back() == '\t' || currentLine.back() == ' ')
		currentLine.erase(currentLine.length() - 1, 1); //erase it
}

} /* namespace emulation */
