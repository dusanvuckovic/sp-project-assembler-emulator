#ifndef IO_H_
#define IO_H_
#include <stdint.h>
#include <map>

namespace emulation {

using byte = uint8_t;
using hword = int16_t;
using word = int32_t;
using uword = uint32_t;

class IO {
	private:
		uword maximumSize;
		std::map<uword, byte> io;
	public:
		IO(uword maximumSize): maximumSize(maximumSize) {}

		word getIO(word);
		void setIO(word, word);
		byte getByteIO(word);
		void setByteIO(word, byte);
};

} /* namespace emulation */

#endif /* IO_H_ */
